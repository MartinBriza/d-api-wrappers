extern (C) {
    void glClearIndex( float c );

    void glClearColor( float red, float green, float blue, float alpha );

    void glClear( uint mask );

    void glIndexMask( uint mask );

    void glColorMask( ubyte red, ubyte green, ubyte blue, ubyte alpha );

    void glAlphaFunc( uint func, float reference );

    void glBlendFunc( uint sfactor, uint dfactor );

    void glLogicOp( uint opcode );

    void glCullFace( uint mode );

    void glFrontFace( uint mode );

    void glPointSize( float size );

    void glLineWidth( float width );

    void glLineStipple( int factor, ushort pattern );

    void glPolygonMode( uint face, uint mode );

    void glPolygonOffset( float factor, float units );

    void glPolygonStipple( const ubyte *mask );

    void glGetPolygonStipple( ubyte *mask );

    void glEdgeFlag( ubyte flag );

    void glEdgeFlagv( const ubyte *flag );

    void glScissor( int x, int y, int width, int height);

    void glClipPlane( uint plane, const double *equation );

    void glGetClipPlane( uint plane, double *equation );

    void glDrawBuffer( uint mode );

    void glReadBuffer( uint mode );

    void glEnable( uint cap );

    void glDisable( uint cap );

    ubyte glIsEnabled( uint cap );


    void glEnableClientState( uint cap );  /* 1.1 */

    void glDisableClientState( uint cap );  /* 1.1 */


    void glGetBooleanv( uint pname, ubyte *params );

    void glGetDoublev( uint pname, double *params );

    void glGetFloatv( uint pname, float *params );

    void glGetIntegerv( uint pname, int *params );


    void glPushAttrib( uint mask );

    void glPopAttrib( );


    void glPushClientAttrib( uint mask );  /* 1.1 */

    void glPopClientAttrib( );  /* 1.1 */


    int glRenderMode( uint mode );

    uint glGetError( );

    ubyte * glGetString( uint name );

    void glFinish( );

    void glFlush( );

    void glHint( uint target, uint mode );


    /*
     * Depth Buffer
     */

    void glClearDepth( double depth );

    void glDepthFunc( uint func );

    void glDepthMask( ubyte flag );

    void glDepthRange( double near_val, double far_val );


    /*
     * Accumulation Buffer
     */

    void glClearAccum( float red, float green, float blue, float alpha );

    void glAccum( uint op, float value );


    /*
     * Transformation
     */

    void glMatrixMode( uint mode );

    void glOrtho( double left, double right,
                  double bottom, double top,
                  double near_val, double far_val );

    void glFrustum( double left, double right,
                    double bottom, double top,
                    double near_val, double far_val );

    void glViewport( int x, int y,
                     int width, int height );

    void glPushMatrix( );

    void glPopMatrix( );

    void glLoadIdentity( );

    void glLoadMatrixd( const double *m );
    void glLoadMatrixf( const float *m );

    void glMultMatrixd( const double *m );
    void glMultMatrixf( const float *m );

    void glRotated( double angle,
                    double x, double y, double z );
    void glRotatef( float angle,
                    float x, float y, float z );

    void glScaled( double x, double y, double z );
    void glScalef( float x, float y, float z );

    void glTranslated( double x, double y, double z );
    void glTranslatef( float x, float y, float z );


    /*
     * Display Lists
     */

    ubyte glIsList( uint list );

    void glDeleteLists( uint list, int range );

    uint glGenLists( int range );

    void glNewList( uint list, uint mode );

    void glEndList(  );

    void glCallList( uint list );

    void glCallLists( int n, uint type,
                      const void *lists );

    void glListBase( uint base );


    /*
     * Drawing Functions
     */

    void glBegin( uint mode );

    void glEnd( );


    void glVertex2d( double x, double y );
    void glVertex2f( float x, float y );
    void glVertex2i( int x, int y );
    void glVertex2s( short x, short y );

    void glVertex3d( double x, double y, double z );
    void glVertex3f( float x, float y, float z );
    void glVertex3i( int x, int y, int z );
    void glVertex3s( short x, short y, short z );

    void glVertex4d( double x, double y, double z, double w );
    void glVertex4f( float x, float y, float z, float w );
    void glVertex4i( int x, int y, int z, int w );
    void glVertex4s( short x, short y, short z, short w );

    void glVertex2dv( const double *v );
    void glVertex2fv( const float *v );
    void glVertex2iv( const int *v );
    void glVertex2sv( const short *v );

    void glVertex3dv( const double *v );
    void glVertex3fv( const float *v );
    void glVertex3iv( const int *v );
    void glVertex3sv( const short *v );

    void glVertex4dv( const double *v );
    void glVertex4fv( const float *v );
    void glVertex4iv( const int *v );
    void glVertex4sv( const short *v );


    void glNormal3b( byte nx, byte ny, byte nz );
    void glNormal3d( double nx, double ny, double nz );
    void glNormal3f( float nx, float ny, float nz );
    void glNormal3i( int nx, int ny, int nz );
    void glNormal3s( short nx, short ny, short nz );

    void glNormal3bv( const byte *v );
    void glNormal3dv( const double *v );
    void glNormal3fv( const float *v );
    void glNormal3iv( const int *v );
    void glNormal3sv( const short *v );


    void glIndexd( double c );
    void glIndexf( float c );
    void glIndexi( int c );
    void glIndexs( short c );
    void glIndexub( ubyte c );  /* 1.1 */

    void glIndexdv( const double *c );
    void glIndexfv( const float *c );
    void glIndexiv( const int *c );
    void glIndexsv( const short *c );
    void glIndexubv( const ubyte *c );  /* 1.1 */

    void glColor3b( byte red, byte green, byte blue );
    void glColor3d( double red, double green, double blue );
    void glColor3f( float red, float green, float blue );
    void glColor3i( int red, int green, int blue );
    void glColor3s( short red, short green, short blue );
    void glColor3ub( ubyte red, ubyte green, ubyte blue );
    void glColor3ui( uint red, uint green, uint blue );
    void glColor3us( ushort red, ushort green, ushort blue );

    void glColor4b( byte red, byte green,
                    byte blue, byte alpha );
    void glColor4d( double red, double green,
                    double blue, double alpha );
    void glColor4f( float red, float green,
                    float blue, float alpha );
    void glColor4i( int red, int green,
                    int blue, int alpha );
    void glColor4s( short red, short green,
                    short blue, short alpha );
    void glColor4ub( ubyte red, ubyte green,
                     ubyte blue, ubyte alpha );
    void glColor4ui( uint red, uint green,
                     uint blue, uint alpha );
    void glColor4us( ushort red, ushort green,
                     ushort blue, ushort alpha );


    void glColor3bv( const byte *v );
    void glColor3dv( const double *v );
    void glColor3fv( const float *v );
    void glColor3iv( const int *v );
    void glColor3sv( const short *v );
    void glColor3ubv( const ubyte *v );
    void glColor3uiv( const uint *v );
    void glColor3usv( const ushort *v );

    void glColor4bv( const byte *v );
    void glColor4dv( const double *v );
    void glColor4fv( const float *v );
    void glColor4iv( const int *v );
    void glColor4sv( const short *v );
    void glColor4ubv( const ubyte *v );
    void glColor4uiv( const uint *v );
    void glColor4usv( const ushort *v );


    void glTexCoord1d( double s );
    void glTexCoord1f( float s );
    void glTexCoord1i( int s );
    void glTexCoord1s( short s );

    void glTexCoord2d( double s, double t );
    void glTexCoord2f( float s, float t );
    void glTexCoord2i( int s, int t );
    void glTexCoord2s( short s, short t );

    void glTexCoord3d( double s, double t, double r );
    void glTexCoord3f( float s, float t, float r );
    void glTexCoord3i( int s, int t, int r );
    void glTexCoord3s( short s, short t, short r );

    void glTexCoord4d( double s, double t, double r, double q );
    void glTexCoord4f( float s, float t, float r, float q );
    void glTexCoord4i( int s, int t, int r, int q );
    void glTexCoord4s( short s, short t, short r, short q );

    void glTexCoord1dv( const double *v );
    void glTexCoord1fv( const float *v );
    void glTexCoord1iv( const int *v );
    void glTexCoord1sv( const short *v );

    void glTexCoord2dv( const double *v );
    void glTexCoord2fv( const float *v );
    void glTexCoord2iv( const int *v );
    void glTexCoord2sv( const short *v );

    void glTexCoord3dv( const double *v );
    void glTexCoord3fv( const float *v );
    void glTexCoord3iv( const int *v );
    void glTexCoord3sv( const short *v );

    void glTexCoord4dv( const double *v );
    void glTexCoord4fv( const float *v );
    void glTexCoord4iv( const int *v );
    void glTexCoord4sv( const short *v );


    void glRasterPos2d( double x, double y );
    void glRasterPos2f( float x, float y );
    void glRasterPos2i( int x, int y );
    void glRasterPos2s( short x, short y );

    void glRasterPos3d( double x, double y, double z );
    void glRasterPos3f( float x, float y, float z );
    void glRasterPos3i( int x, int y, int z );
    void glRasterPos3s( short x, short y, short z );

    void glRasterPos4d( double x, double y, double z, double w );
    void glRasterPos4f( float x, float y, float z, float w );
    void glRasterPos4i( int x, int y, int z, int w );
    void glRasterPos4s( short x, short y, short z, short w );

    void glRasterPos2dv( const double *v );
    void glRasterPos2fv( const float *v );
    void glRasterPos2iv( const int *v );
    void glRasterPos2sv( const short *v );

    void glRasterPos3dv( const double *v );
    void glRasterPos3fv( const float *v );
    void glRasterPos3iv( const int *v );
    void glRasterPos3sv( const short *v );

    void glRasterPos4dv( const double *v );
    void glRasterPos4fv( const float *v );
    void glRasterPos4iv( const int *v );
    void glRasterPos4sv( const short *v );


    void glRectd( double x1, double y1, double x2, double y2 );
    void glRectf( float x1, float y1, float x2, float y2 );
    void glRecti( int x1, int y1, int x2, int y2 );
    void glRects( short x1, short y1, short x2, short y2 );


    void glRectdv( const double *v1, const double *v2 );
    void glRectfv( const float *v1, const float *v2 );
    void glRectiv( const int *v1, const int *v2 );
    void glRectsv( const short *v1, const short *v2 );


    /*
     * Vertex Arrays  (1.1)
     */

    void glVertexPointer( int size, uint type,
                          int stride, const void *ptr );

    void glNormalPointer( uint type, int stride,
                          const void *ptr );

    void glColorPointer( int size, uint type,
                         int stride, const void *ptr );

    void glIndexPointer( uint type, int stride,
                         const void *ptr );

    void glTexCoordPointer( int size, uint type,
                            int stride, const void *ptr );

    void glEdgeFlagPointer( int stride, const void *ptr );

    void glGetPointerv( uint pname, void **params );

    void glArrayElement( int i );

    void glDrawArrays( uint mode, int first, int count );

    void glDrawElements( uint mode, int count,
                         uint type, const void *indices );

    void glInterleavedArrays( uint format, int stride,
                              const void *pointer );

    /*
     * Lighting
     */

    void glShadeModel( uint mode );

    void glLightf( uint light, uint pname, float param );
    void glLighti( uint light, uint pname, int param );
    void glLightfv( uint light, uint pname,
                    const float *params );
    void glLightiv( uint light, uint pname,
                    const int *params );

    void glGetLightfv( uint light, uint pname,
                       float *params );
    void glGetLightiv( uint light, uint pname,
                       int *params );

    void glLightModelf( uint pname, float param );
    void glLightModeli( uint pname, int param );
    void glLightModelfv( uint pname, const float *params );
    void glLightModeliv( uint pname, const int *params );

    void glMaterialf( uint face, uint pname, float param );
    void glMateriali( uint face, uint pname, int param );
    void glMaterialfv( uint face, uint pname, const float *params );
    void glMaterialiv( uint face, uint pname, const int *params );

    void glGetMaterialfv( uint face, uint pname, float *params );
    void glGetMaterialiv( uint face, uint pname, int *params );

    void glColorMaterial( uint face, uint mode );


    /*
     * Raster functions
     */

    void glPixelZoom( float xfactor, float yfactor );

    void glPixelStoref( uint pname, float param );
    void glPixelStorei( uint pname, int param );

    void glPixelTransferf( uint pname, float param );
    void glPixelTransferi( uint pname, int param );

    void glPixelMapfv( uint map, int mapsize,
                       const float *values );
    void glPixelMapuiv( uint map, int mapsize,
                        const uint *values );
    void glPixelMapusv( uint map, int mapsize,
                        const ushort *values );

    void glGetPixelMapfv( uint map, float *values );
    void glGetPixelMapuiv( uint map, uint *values );
    void glGetPixelMapusv( uint map, ushort *values );

    void glBitmap( int width, int height,
                   float xorig, float yorig,
                   float xmove, float ymove,
                   const ubyte *bitmap );

    void glReadPixels( int x, int y,
                       int width, int height,
                       uint format, uint type,
                       void *pixels );

    void glDrawPixels( int width, int height,
                       uint format, uint type,
                       const void *pixels );

    void glCopyPixels( int x, int y,
                       int width, int height,
                       uint type );

    /*
     * Stenciling
     */

    void glStencilFunc( uint func, int reference, uint mask );

    void glStencilMask( uint mask );

    void glStencilOp( uint fail, uint zfail, uint zpass );

    void glClearStencil( int s );



    /*
     * Texture mapping
     */

    void glTexGend( uint coord, uint pname, double param );
    void glTexGenf( uint coord, uint pname, float param );
    void glTexGeni( uint coord, uint pname, int param );

    void glTexGendv( uint coord, uint pname, const double *params );
    void glTexGenfv( uint coord, uint pname, const float *params );
    void glTexGeniv( uint coord, uint pname, const int *params );

    void glGetTexGendv( uint coord, uint pname, double *params );
    void glGetTexGenfv( uint coord, uint pname, float *params );
    void glGetTexGeniv( uint coord, uint pname, int *params );


    void glTexEnvf( uint target, uint pname, float param );
    void glTexEnvi( uint target, uint pname, int param );

    void glTexEnvfv( uint target, uint pname, const float *params );
    void glTexEnviv( uint target, uint pname, const int *params );

    void glGetTexEnvfv( uint target, uint pname, float *params );
    void glGetTexEnviv( uint target, uint pname, int *params );


    void glTexParameterf( uint target, uint pname, float param );
    void glTexParameteri( uint target, uint pname, int param );

    void glTexParameterfv( uint target, uint pname,
                           const float *params );
    void glTexParameteriv( uint target, uint pname,
                           const int *params );

    void glGetTexParameterfv( uint target,
                              uint pname, float *params);
    void glGetTexParameteriv( uint target,
                              uint pname, int *params );

    void glGetTexLevelParameterfv( uint target, int level,
                                   uint pname, float *params );
    void glGetTexLevelParameteriv( uint target, int level,
                                   uint pname, int *params );


    void glTexImage1D( uint target, int level,
                       int internalFormat,
                       int width, int border,
                       uint format, uint type,
                       const void *pixels );

    void glTexImage2D( uint target, int level,
                       int internalFormat,
                       int width, int height,
                       int border, uint format, uint type,
                       const void *pixels );

    void glGetTexImage( uint target, int level,
                        uint format, uint type,
                        void *pixels );


    /* 1.1 functions */

    void glGenTextures( int n, uint *textures );

    void glDeleteTextures( int n, const uint *textures);

    void glBindTexture( uint target, uint texture );

    void glPrioritizeTextures( int n,
                               const uint *textures,
                               const float *priorities );

    ubyte glAreTexturesResident( int n,
                                 const uint *textures,
                                 ubyte *residences );

    ubyte glIsTexture( uint texture );


    void glTexSubImage1D( uint target, int level,
                          int xoffset,
                          int width, uint format,
                          uint type, const void *pixels );


    void glTexSubImage2D( uint target, int level,
                          int xoffset, int yoffset,
                          int width, int height,
                          uint format, uint type,
                          const void *pixels );


    void glCopyTexImage1D( uint target, int level,
                           uint internalformat,
                           int x, int y,
                           int width, int border );


    void glCopyTexImage2D( uint target, int level,
                           uint internalformat,
                           int x, int y,
                           int width, int height,
                           int border );


    void glCopyTexSubImage1D( uint target, int level,
                              int xoffset, int x, int y,
                              int width );


    void glCopyTexSubImage2D( uint target, int level,
                              int xoffset, int yoffset,
                              int x, int y,
                              int width, int height );


    /*
     * Evaluators
     */

    void glMap1d( uint target, double u1, double u2,
                  int stride,
                  int order, const double *points );
    void glMap1f( uint target, float u1, float u2,
                  int stride,
                  int order, const float *points );

    void glMap2d( uint target,
                  double u1, double u2, int ustride, int uorder,
                  double v1, double v2, int vstride, int vorder,
                  const double *points );
    void glMap2f( uint target,
                  float u1, float u2, int ustride, int uorder,
                  float v1, float v2, int vstride, int vorder,
                  const float *points );

    void glGetMapdv( uint target, uint query, double *v );
    void glGetMapfv( uint target, uint query, float *v );
    void glGetMapiv( uint target, uint query, int *v );

    void glEvalCoord1d( double u );
    void glEvalCoord1f( float u );

    void glEvalCoord1dv( const double *u );
    void glEvalCoord1fv( const float *u );

    void glEvalCoord2d( double u, double v );
    void glEvalCoord2f( float u, float v );

    void glEvalCoord2dv( const double *u );
    void glEvalCoord2fv( const float *u );

    void glMapGrid1d( int un, double u1, double u2 );
    void glMapGrid1f( int un, float u1, float u2 );

    void glMapGrid2d( int un, double u1, double u2,
                      int vn, double v1, double v2 );
    void glMapGrid2f( int un, float u1, float u2,
                      int vn, float v1, float v2 );

    void glEvalPoint1( int i );

    void glEvalPoint2( int i, int j );

    void glEvalMesh1( uint mode, int i1, int i2 );

    void glEvalMesh2( uint mode, int i1, int i2, int j1, int j2 );


    /*
     * Fog
     */

    void glFogf( uint pname, float param );

    void glFogi( uint pname, int param );

    void glFogfv( uint pname, const float *params );

    void glFogiv( uint pname, const int *params );


    /*
     * Selection and Feedback
     */

    void glFeedbackBuffer( int size, uint type, float *buffer );

    void glPassThrough( float token );

    void glSelectBuffer( int size, uint *buffer );

    void glInitNames( );

    void glLoadName( uint name );

    void glPushName( uint name );

    void glPopName( );



    /*
     * OpenGL 1.2
     */



    void glDrawRangeElements( uint mode, uint start,
                              uint end, int count, uint type, const void *indices );

    void glTexImage3D( uint target, int level,
                       int internalFormat,
                       int width, int height,
                       int depth, int border,
                       uint format, uint type,
                       const void *pixels );

    void glTexSubImage3D( uint target, int level,
                          int xoffset, int yoffset,
                          int zoffset, int width,
                          int height, int depth,
                          uint format,
                          uint type, const void *pixels);

    void glCopyTexSubImage3D( uint target, int level,
                              int xoffset, int yoffset,
                              int zoffset, int x,
                              int y, int width,
                              int height );

    /*
     * GL_ARB_imaging
     */




    void glColorTable( uint target, uint internalformat,
                       int width, uint format,
                       uint type, const void *table );

    void glColorSubTable( uint target,
                          int start, int count,
                          uint format, uint type,
                          const void *data );

    void glColorTableParameteriv(uint target, uint pname,
                                 const int *params);

    void glColorTableParameterfv(uint target, uint pname,
                                 const float *params);

    void glCopyColorSubTable( uint target, int start,
                              int x, int y, int width );

    void glCopyColorTable( uint target, uint internalformat,
                           int x, int y, int width );

    void glGetColorTable( uint target, uint format,
                          uint type, void *table );

    void glGetColorTableParameterfv( uint target, uint pname,
                                     float *params );

    void glGetColorTableParameteriv( uint target, uint pname,
                                     int *params );

    void glBlendEquation( uint mode );

    void glBlendColor( float red, float green,
                       float blue, float alpha );

    void glHistogram( uint target, int width,
                      uint internalformat, ubyte sink );

    void glResetHistogram( uint target );

    void glGetHistogram( uint target, ubyte reset,
                         uint format, uint type,
                         void *values );

    void glGetHistogramParameterfv( uint target, uint pname,
                                    float *params );

    void glGetHistogramParameteriv( uint target, uint pname,
                                    int *params );

    void glMinmax( uint target, uint internalformat,
                   ubyte sink );

    void glResetMinmax( uint target );

    void glGetMinmax( uint target, ubyte reset,
                      uint format, uint types,
                      void *values );

    void glGetMinmaxParameterfv( uint target, uint pname,
                                 float *params );

    void glGetMinmaxParameteriv( uint target, uint pname,
                                 int *params );

    void glConvolutionFilter1D( uint target,
                                uint internalformat, int width, uint format, uint type,
                                const void *image );

    void glConvolutionFilter2D( uint target,
                                uint internalformat, int width, int height, uint format,
                                uint type, const void *image );

    void glConvolutionParameterf( uint target, uint pname,
                                  float params );

    void glConvolutionParameterfv( uint target, uint pname,
                                   const float *params );

    void glConvolutionParameteri( uint target, uint pname,
                                  int params );

    void glConvolutionParameteriv( uint target, uint pname,
                                   const int *params );

    void glCopyConvolutionFilter1D( uint target,
                                    uint internalformat, int x, int y, int width );

    void glCopyConvolutionFilter2D( uint target,
                                    uint internalformat, int x, int y, int width,
                                    int height);

    void glGetConvolutionFilter( uint target, uint format,
                                 uint type, void *image );

    void glGetConvolutionParameterfv( uint target, uint pname,
                                      float *params );

    void glGetConvolutionParameteriv( uint target, uint pname,
                                      int *params );

    void glSeparableFilter2D( uint target,
                              uint internalformat, int width, int height, uint format,
                              uint type, const void *row, const void *column );

    void glGetSeparableFilter( uint target, uint format,
                               uint type, void *row, void *column, void *span );

    /*
     * OpenGL 1.3
     */



    void glActiveTexture( uint texture );

    void glClientActiveTexture( uint texture );

    void glCompressedTexImage1D( uint target, int level, uint internalformat, int width, int border, int imageSize, const void *data );

    void glCompressedTexImage2D( uint target, int level, uint internalformat, int width, int height, int border, int imageSize, const void *data );

    void glCompressedTexImage3D( uint target, int level, uint internalformat, int width, int height, int depth, int border, int imageSize, const void *data );

    void glCompressedTexSubImage1D( uint target, int level, int xoffset, int width, uint format, int imageSize, const void *data );

    void glCompressedTexSubImage2D( uint target, int level, int xoffset, int yoffset, int width, int height, uint format, int imageSize, const void *data );

    void glCompressedTexSubImage3D( uint target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, uint format, int imageSize, const void *data );

    void glGetCompressedTexImage( uint target, int lod, void *img );



    /* Boolean values */
    const ushort GL_FALSE = 0x0;
    const ushort GL_TRUE = 0x1;

    /* Data types */
    const ushort GL_BYTE = 0x1400;
    const ushort GL_UNSIGNED_BYTE = 0x1401;
    const ushort GL_SHORT = 0x1402;
    const ushort GL_UNSIGNED_SHORT = 0x1403;
    const ushort GL_INT = 0x1404;
    const ushort GL_UNSIGNED_INT = 0x1405;
    const ushort GL_FLOAT = 0x1406;
    const ushort GL_2_BYTES = 0x1407;
    const ushort GL_3_BYTES = 0x1408;
    const ushort GL_4_BYTES = 0x1409;
    const ushort GL_DOUBLE = 0x140A;

    /* Primitives */
    const ushort GL_POINTS = 0x0000;
    const ushort GL_LINES = 0x0001;
    const ushort GL_LINE_LOOP = 0x0002;
    const ushort GL_LINE_STRIP = 0x0003;
    const ushort GL_TRIANGLES = 0x0004;
    const ushort GL_TRIANGLE_STRIP = 0x0005;
    const ushort GL_TRIANGLE_FAN = 0x0006;
    const ushort GL_QUADS = 0x0007;
    const ushort GL_QUAD_STRIP = 0x0008;
    const ushort GL_POLYGON = 0x0009;

    /* Vertex Arrays */
    const ushort GL_VERTEX_ARRAY = 0x8074;
    const ushort GL_NORMAL_ARRAY = 0x8075;
    const ushort GL_COLOR_ARRAY = 0x8076;
    const ushort GL_INDEX_ARRAY = 0x8077;
    const ushort GL_TEXTURE_COORD_ARRAY = 0x8078;
    const ushort GL_EDGE_FLAG_ARRAY = 0x8079;
    const ushort GL_VERTEX_ARRAY_SIZE = 0x807A;
    const ushort GL_VERTEX_ARRAY_TYPE = 0x807B;
    const ushort GL_VERTEX_ARRAY_STRIDE = 0x807C;
    const ushort GL_NORMAL_ARRAY_TYPE = 0x807E;
    const ushort GL_NORMAL_ARRAY_STRIDE = 0x807F;
    const ushort GL_COLOR_ARRAY_SIZE = 0x8081;
    const ushort GL_COLOR_ARRAY_TYPE = 0x8082;
    const ushort GL_COLOR_ARRAY_STRIDE = 0x8083;
    const ushort GL_INDEX_ARRAY_TYPE = 0x8085;
    const ushort GL_INDEX_ARRAY_STRIDE = 0x8086;
    const ushort GL_TEXTURE_COORD_ARRAY_SIZE = 0x8088;
    const ushort GL_TEXTURE_COORD_ARRAY_TYPE = 0x8089;
    const ushort GL_TEXTURE_COORD_ARRAY_STRIDE = 0x808A;
    const ushort GL_EDGE_FLAG_ARRAY_STRIDE = 0x808C;
    const ushort GL_VERTEX_ARRAY_POINTER = 0x808E;
    const ushort GL_NORMAL_ARRAY_POINTER = 0x808F;
    const ushort GL_COLOR_ARRAY_POINTER = 0x8090;
    const ushort GL_INDEX_ARRAY_POINTER = 0x8091;
    const ushort GL_TEXTURE_COORD_ARRAY_POINTER = 0x8092;
    const ushort GL_EDGE_FLAG_ARRAY_POINTER = 0x8093;
    const ushort GL_V2F = 0x2A20;
    const ushort GL_V3F = 0x2A21;
    const ushort GL_C4UB_V2F = 0x2A22;
    const ushort GL_C4UB_V3F = 0x2A23;
    const ushort GL_C3F_V3F = 0x2A24;
    const ushort GL_N3F_V3F = 0x2A25;
    const ushort GL_C4F_N3F_V3F = 0x2A26;
    const ushort GL_T2F_V3F = 0x2A27;
    const ushort GL_T4F_V4F = 0x2A28;
    const ushort GL_T2F_C4UB_V3F = 0x2A29;
    const ushort GL_T2F_C3F_V3F = 0x2A2A;
    const ushort GL_T2F_N3F_V3F = 0x2A2B;
    const ushort GL_T2F_C4F_N3F_V3F = 0x2A2C;
    const ushort GL_T4F_C4F_N3F_V4F = 0x2A2D;

    /* Matrix Mode */
    const ushort GL_MATRIX_MODE = 0x0BA0;
    const ushort GL_MODELVIEW = 0x1700;
    const ushort GL_PROJECTION = 0x1701;
    const ushort GL_TEXTURE = 0x1702;

    /* Points */
    const ushort GL_POINT_SMOOTH = 0x0B10;
    const ushort GL_POINT_SIZE = 0x0B11;
    const ushort GL_POINT_SIZE_GRANULARITY = 0x0B13;
    const ushort GL_POINT_SIZE_RANGE = 0x0B12;

    /* Lines */
    const ushort GL_LINE_SMOOTH = 0x0B20;
    const ushort GL_LINE_STIPPLE = 0x0B24;
    const ushort GL_LINE_STIPPLE_PATTERN = 0x0B25;
    const ushort GL_LINE_STIPPLE_REPEAT = 0x0B26;
    const ushort GL_LINE_WIDTH = 0x0B21;
    const ushort GL_LINE_WIDTH_GRANULARITY = 0x0B23;
    const ushort GL_LINE_WIDTH_RANGE = 0x0B22;

    /* Polygons */
    const ushort GL_POINT = 0x1B00;
    const ushort GL_LINE = 0x1B01;
    const ushort GL_FILL = 0x1B02;
    const ushort GL_CW = 0x0900;
    const ushort GL_CCW = 0x0901;
    const ushort GL_FRONT = 0x0404;
    const ushort GL_BACK = 0x0405;
    const ushort GL_POLYGON_MODE = 0x0B40;
    const ushort GL_POLYGON_SMOOTH = 0x0B41;
    const ushort GL_POLYGON_STIPPLE = 0x0B42;
    const ushort GL_EDGE_FLAG = 0x0B43;
    const ushort GL_CULL_FACE = 0x0B44;
    const ushort GL_CULL_FACE_MODE = 0x0B45;
    const ushort GL_FRONT_FACE = 0x0B46;
    const ushort GL_POLYGON_OFFSET_FACTOR = 0x8038;
    const ushort GL_POLYGON_OFFSET_UNITS = 0x2A00;
    const ushort GL_POLYGON_OFFSET_POINT = 0x2A01;
    const ushort GL_POLYGON_OFFSET_LINE = 0x2A02;
    const ushort GL_POLYGON_OFFSET_FILL = 0x8037;

    /* Display Lists */
    const ushort GL_COMPILE = 0x1300;
    const ushort GL_COMPILE_AND_EXECUTE = 0x1301;
    const ushort GL_LIST_BASE = 0x0B32;
    const ushort GL_LIST_INDEX = 0x0B33;
    const ushort GL_LIST_MODE = 0x0B30;

    /* Depth buffer */
    const ushort GL_NEVER = 0x0200;
    const ushort GL_LESS = 0x0201;
    const ushort GL_EQUAL = 0x0202;
    const ushort GL_LEQUAL = 0x0203;
    const ushort GL_GREATER = 0x0204;
    const ushort GL_NOTEQUAL = 0x0205;
    const ushort GL_GEQUAL = 0x0206;
    const ushort GL_ALWAYS = 0x0207;
    const ushort GL_DEPTH_TEST = 0x0B71;
    const ushort GL_DEPTH_BITS = 0x0D56;
    const ushort GL_DEPTH_CLEAR_VALUE = 0x0B73;
    const ushort GL_DEPTH_FUNC = 0x0B74;
    const ushort GL_DEPTH_RANGE = 0x0B70;
    const ushort GL_DEPTH_WRITEMASK = 0x0B72;
    const ushort GL_DEPTH_COMPONENT = 0x1902;

    /* Lighting */
    const ushort GL_LIGHTING = 0x0B50;
    const ushort GL_LIGHT0 = 0x4000;
    const ushort GL_LIGHT1 = 0x4001;
    const ushort GL_LIGHT2 = 0x4002;
    const ushort GL_LIGHT3 = 0x4003;
    const ushort GL_LIGHT4 = 0x4004;
    const ushort GL_LIGHT5 = 0x4005;
    const ushort GL_LIGHT6 = 0x4006;
    const ushort GL_LIGHT7 = 0x4007;
    const ushort GL_SPOT_EXPONENT = 0x1205;
    const ushort GL_SPOT_CUTOFF = 0x1206;
    const ushort GL_CONSTANT_ATTENUATION = 0x1207;
    const ushort GL_LINEAR_ATTENUATION = 0x1208;
    const ushort GL_QUADRATIC_ATTENUATION = 0x1209;
    const ushort GL_AMBIENT = 0x1200;
    const ushort GL_DIFFUSE = 0x1201;
    const ushort GL_SPECULAR = 0x1202;
    const ushort GL_SHININESS = 0x1601;
    const ushort GL_EMISSION = 0x1600;
    const ushort GL_POSITION = 0x1203;
    const ushort GL_SPOT_DIRECTION = 0x1204;
    const ushort GL_AMBIENT_AND_DIFFUSE = 0x1602;
    const ushort GL_COLOR_INDEXES = 0x1603;
    const ushort GL_LIGHT_MODEL_TWO_SIDE = 0x0B52;
    const ushort GL_LIGHT_MODEL_LOCAL_VIEWER = 0x0B51;
    const ushort GL_LIGHT_MODEL_AMBIENT = 0x0B53;
    const ushort GL_FRONT_AND_BACK = 0x0408;
    const ushort GL_SHADE_MODEL = 0x0B54;
    const ushort GL_FLAT = 0x1D00;
    const ushort GL_SMOOTH = 0x1D01;
    const ushort GL_COLOR_MATERIAL = 0x0B57;
    const ushort GL_COLOR_MATERIAL_FACE = 0x0B55;
    const ushort GL_COLOR_MATERIAL_PARAMETER = 0x0B56;
    const ushort GL_NORMALIZE = 0x0BA1;

    /* User clipping planes */
    const ushort GL_CLIP_PLANE0 = 0x3000;
    const ushort GL_CLIP_PLANE1 = 0x3001;
    const ushort GL_CLIP_PLANE2 = 0x3002;
    const ushort GL_CLIP_PLANE3 = 0x3003;
    const ushort GL_CLIP_PLANE4 = 0x3004;
    const ushort GL_CLIP_PLANE5 = 0x3005;

    /* Accumulation buffer */
    const ushort GL_ACCUM_RED_BITS = 0x0D58;
    const ushort GL_ACCUM_GREEN_BITS = 0x0D59;
    const ushort GL_ACCUM_BLUE_BITS = 0x0D5A;
    const ushort GL_ACCUM_ALPHA_BITS = 0x0D5B;
    const ushort GL_ACCUM_CLEAR_VALUE = 0x0B80;
    const ushort GL_ACCUM = 0x0100;
    const ushort GL_ADD = 0x0104;
    const ushort GL_LOAD = 0x0101;
    const ushort GL_MULT = 0x0103;
    const ushort GL_RETURN = 0x0102;

    /* Alpha testing */
    const ushort GL_ALPHA_TEST = 0x0BC0;
    const ushort GL_ALPHA_TEST_REF = 0x0BC2;
    const ushort GL_ALPHA_TEST_FUNC = 0x0BC1;

    /* Blending */
    const ushort GL_BLEND = 0x0BE2;
    const ushort GL_BLEND_SRC = 0x0BE1;
    const ushort GL_BLEND_DST = 0x0BE0;
    const ushort GL_ZERO = 0x0;
    const ushort GL_ONE = 0x1;
    const ushort GL_SRC_COLOR = 0x0300;
    const ushort GL_ONE_MINUS_SRC_COLOR = 0x0301;
    const ushort GL_SRC_ALPHA = 0x0302;
    const ushort GL_ONE_MINUS_SRC_ALPHA = 0x0303;
    const ushort GL_DST_ALPHA = 0x0304;
    const ushort GL_ONE_MINUS_DST_ALPHA = 0x0305;
    const ushort GL_DST_COLOR = 0x0306;
    const ushort GL_ONE_MINUS_DST_COLOR = 0x0307;
    const ushort GL_SRC_ALPHA_SATURATE = 0x0308;

    /* Render Mode */
    const ushort GL_FEEDBACK = 0x1C01;
    const ushort GL_RENDER = 0x1C00;
    const ushort GL_SELECT = 0x1C02;

    /* Feedback */
    const ushort GL_2D = 0x0600;
    const ushort GL_3D = 0x0601;
    const ushort GL_3D_COLOR = 0x0602;
    const ushort GL_3D_COLOR_TEXTURE = 0x0603;
    const ushort GL_4D_COLOR_TEXTURE = 0x0604;
    const ushort GL_POINT_TOKEN = 0x0701;
    const ushort GL_LINE_TOKEN = 0x0702;
    const ushort GL_LINE_RESET_TOKEN = 0x0707;
    const ushort GL_POLYGON_TOKEN = 0x0703;
    const ushort GL_BITMAP_TOKEN = 0x0704;
    const ushort N = 0x0705;
    const ushort GL_COPY_PIXEL_TOKEN = 0x0706;
    const ushort GL_PASS_THROUGH_TOKEN = 0x0700;
    const ushort GL_FEEDBACK_BUFFER_POINTER = 0x0DF0;
    const ushort GL_FEEDBACK_BUFFER_SIZE = 0x0DF1;
    const ushort GL_FEEDBACK_BUFFER_TYPE = 0x0DF2;

    /* Selection */
    const ushort GL_SELECTION_BUFFER_POINTER = 0x0DF3;
    const ushort GL_SELECTION_BUFFER_SIZE = 0x0DF4;

    /* Fog */
    const ushort GL_FOG = 0x0B60;
    const ushort GL_FOG_MODE = 0x0B65;
    const ushort GL_FOG_DENSITY = 0x0B62;
    const ushort GL_FOG_COLOR = 0x0B66;
    const ushort GL_FOG_INDEX = 0x0B61;
    const ushort GL_FOG_START = 0x0B63;
    const ushort GL_FOG_END = 0x0B64;
    const ushort GL_LINEAR = 0x2601;
    const ushort GL_EXP = 0x0800;
    const ushort GL_EXP2 = 0x0801;

    /* Logic Ops */
    const ushort GL_LOGIC_OP = 0x0BF1;
    const ushort GL_INDEX_LOGIC_OP = 0x0BF1;
    const ushort GL_COLOR_LOGIC_OP = 0x0BF2;
    const ushort GL_LOGIC_OP_MODE = 0x0BF0;
    const ushort GL_CLEAR = 0x1500;
    const ushort GL_SET = 0x150F;
    const ushort GL_COPY = 0x1503;
    const ushort GL_COPY_INVERTED = 0x150C;
    const ushort GL_NOOP = 0x1505;
    const ushort GL_INVERT = 0x150A;
    const ushort GL_AND = 0x1501;
    const ushort GL_NAND = 0x150E;
    const ushort GL_OR = 0x1507;
    const ushort GL_NOR = 0x1508;
    const ushort GL_XOR = 0x1506;
    const ushort GL_EQUIV = 0x1509;
    const ushort GL_AND_REVERSE = 0x1502;
    const ushort GL_AND_INVERTED = 0x1504;
    const ushort GL_OR_REVERSE = 0x150B;
    const ushort GL_OR_INVERTED = 0x150D;

    /* Stencil */
    const ushort GL_STENCIL_BITS = 0x0D57;
    const ushort GL_STENCIL_TEST = 0x0B90;
    const ushort GL_STENCIL_CLEAR_VALUE = 0x0B91;
    const ushort GL_STENCIL_FUNC = 0x0B92;
    const ushort GL_STENCIL_VALUE_MASK = 0x0B93;
    const ushort GL_STENCIL_FAIL = 0x0B94;
    const ushort GL_STENCIL_PASS_DEPTH_FAIL = 0x0B95;
    const ushort GL_STENCIL_PASS_DEPTH_PASS = 0x0B96;
    const ushort GL_STENCIL_REF = 0x0B97;
    const ushort GL_STENCIL_WRITEMASK = 0x0B98;
    const ushort GL_STENCIL_INDEX = 0x1901;
    const ushort GL_KEEP = 0x1E00;
    const ushort GL_REPLACE = 0x1E01;
    const ushort GL_INCR = 0x1E02;
    const ushort GL_DECR = 0x1E03;

    /* Buffers, Pixel Drawing/Reading */
    const ushort GL_NONE = 0x0;
    const ushort GL_LEFT = 0x0406;
    const ushort GL_RIGHT = 0x0407;
    /*GL_FRONT                  0x0404 */
    /*GL_BACK                   0x0405 */
    /*GL_FRONT_AND_BACK             0x0408 */
    const ushort GL_FRONT_LEFT = 0x0400;
    const ushort GL_FRONT_RIGHT = 0x0401;
    const ushort GL_BACK_LEFT = 0x0402;
    const ushort GL_BACK_RIGHT = 0x0403;
    const ushort GL_AUX0 = 0x0409;
    const ushort GL_AUX1 = 0x040A;
    const ushort GL_AUX2 = 0x040B;
    const ushort GL_AUX3 = 0x040C;
    const ushort GL_COLOR_INDEX = 0x1900;
    const ushort GL_RED = 0x1903;
    const ushort GL_GREEN = 0x1904;
    const ushort GL_BLUE = 0x1905;
    const ushort GL_ALPHA = 0x1906;
    const ushort GL_LUMINANCE = 0x1909;
    const ushort GL_LUMINANCE_ALPHA = 0x190A;
    const ushort GL_ALPHA_BITS = 0x0D55;
    const ushort GL_RED_BITS = 0x0D52;
    const ushort GL_GREEN_BITS = 0x0D53;
    const ushort GL_BLUE_BITS = 0x0D54;
    const ushort GL_INDEX_BITS = 0x0D51;
    const ushort GL_SUBPIXEL_BITS = 0x0D50;
    const ushort GL_AUX_BUFFERS = 0x0C00;
    const ushort GL_READ_BUFFER = 0x0C02;
    const ushort GL_DRAW_BUFFER = 0x0C01;
    const ushort GL_DOUBLEBUFFER = 0x0C32;
    const ushort GL_STEREO = 0x0C33;
    const ushort GL_BITMAP = 0x1A00;
    const ushort GL_COLOR = 0x1800;
    const ushort GL_DEPTH = 0x1801;
    const ushort GL_STENCIL = 0x1802;
    const ushort GL_DITHER = 0x0BD0;
    const ushort GL_RGB = 0x1907;
    const ushort GL_RGBA = 0x1908;

    /* Implementation limits */
    const ushort GL_MAX_LIST_NESTING = 0x0B31;
    const ushort GL_MAX_EVAL_ORDER = 0x0D30;
    const ushort GL_MAX_LIGHTS = 0x0D31;
    const ushort GL_MAX_CLIP_PLANES = 0x0D32;
    const ushort GL_MAX_TEXTURE_SIZE = 0x0D33;
    const ushort GL_MAX_PIXEL_MAP_TABLE = 0x0D34;
    const ushort GL_MAX_ATTRIB_STACK_DEPTH = 0x0D35;
    const ushort GL_MAX_MODELVIEW_STACK_DEPTH = 0x0D36;
    const ushort GL_MAX_NAME_STACK_DEPTH = 0x0D37;
    const ushort GL_MAX_PROJECTION_STACK_DEPTH = 0x0D38;
    const ushort GL_MAX_TEXTURE_STACK_DEPTH = 0x0D39;
    const ushort GL_MAX_VIEWPORT_DIMS = 0x0D3A;
    const ushort GL_MAX_CLIENT_ATTRIB_STACK_DEPTH = 0x0D3B;

    /* Gets */
    const ushort GL_ATTRIB_STACK_DEPTH = 0x0BB0;
    const ushort GL_CLIENT_ATTRIB_STACK_DEPTH = 0x0BB1;
    const ushort GL_COLOR_CLEAR_VALUE = 0x0C22;
    const ushort GL_COLOR_WRITEMASK = 0x0C23;
    const ushort GL_CURRENT_INDEX = 0x0B01;
    const ushort GL_CURRENT_COLOR = 0x0B00;
    const ushort GL_CURRENT_NORMAL = 0x0B02;
    const ushort GL_CURRENT_RASTER_COLOR = 0x0B04;
    const ushort GL_CURRENT_RASTER_DISTANCE = 0x0B09;
    const ushort GL_CURRENT_RASTER_INDEX = 0x0B05;
    const ushort GL_CURRENT_RASTER_POSITION = 0x0B07;
    const ushort GL_CURRENT_RASTER_TEXTURE_COORDS = 0x0B06;
    const ushort GL_CURRENT_RASTER_POSITION_VALID = 0x0B08;
    const ushort GL_CURRENT_TEXTURE_COORDS = 0x0B03;
    const ushort GL_INDEX_CLEAR_VALUE = 0x0C20;
    const ushort GL_INDEX_MODE = 0x0C30;
    const ushort GL_INDEX_WRITEMASK = 0x0C21;
    const ushort GL_MODELVIEW_MATRIX = 0x0BA6;
    const ushort GL_MODELVIEW_STACK_DEPTH = 0x0BA3;
    const ushort GL_NAME_STACK_DEPTH = 0x0D70;
    const ushort GL_PROJECTION_MATRIX = 0x0BA7;
    const ushort GL_PROJECTION_STACK_DEPTH = 0x0BA4;
    const ushort GL_RENDER_MODE = 0x0C40;
    const ushort GL_RGBA_MODE = 0x0C31;
    const ushort GL_TEXTURE_MATRIX = 0x0BA8;
    const ushort GL_TEXTURE_STACK_DEPTH = 0x0BA5;
    const ushort GL_VIEWPORT = 0x0BA2;

    /* Evaluators */
    const ushort GL_AUTO_NORMAL = 0x0D80;
    const ushort GL_MAP1_COLOR_4 = 0x0D90;
    const ushort GL_MAP1_INDEX = 0x0D91;
    const ushort GL_MAP1_NORMAL = 0x0D92;
    const ushort GL_MAP1_TEXTURE_COORD_1 = 0x0D93;
    const ushort GL_MAP1_TEXTURE_COORD_2 = 0x0D94;
    const ushort GL_MAP1_TEXTURE_COORD_3 = 0x0D95;
    const ushort GL_MAP1_TEXTURE_COORD_4 = 0x0D96;
    const ushort GL_MAP1_VERTEX_3 = 0x0D97;
    const ushort GL_MAP1_VERTEX_4 = 0x0D98;
    const ushort GL_MAP2_COLOR_4 = 0x0DB0;
    const ushort GL_MAP2_INDEX = 0x0DB1;
    const ushort GL_MAP2_NORMAL = 0x0DB2;
    const ushort GL_MAP2_TEXTURE_COORD_1 = 0x0DB3;
    const ushort GL_MAP2_TEXTURE_COORD_2 = 0x0DB4;
    const ushort GL_MAP2_TEXTURE_COORD_3 = 0x0DB5;
    const ushort GL_MAP2_TEXTURE_COORD_4 = 0x0DB6;
    const ushort GL_MAP2_VERTEX_3 = 0x0DB7;
    const ushort GL_MAP2_VERTEX_4 = 0x0DB8;
    const ushort GL_MAP1_GRID_DOMAIN = 0x0DD0;
    const ushort GL_MAP1_GRID_SEGMENTS = 0x0DD1;
    const ushort GL_MAP2_GRID_DOMAIN = 0x0DD2;
    const ushort GL_MAP2_GRID_SEGMENTS = 0x0DD3;
    const ushort GL_COEFF = 0x0A00;
    const ushort GL_ORDER = 0x0A01;
    const ushort GL_DOMAIN = 0x0A02;

    /* Hints */
    const ushort GL_PERSPECTIVE_CORRECTION_HINT = 0x0C50;
    const ushort GL_POINT_SMOOTH_HINT = 0x0C51;
    const ushort GL_LINE_SMOOTH_HINT = 0x0C52;
    const ushort GL_POLYGON_SMOOTH_HINT = 0x0C53;
    const ushort GL_FOG_HINT = 0x0C54;
    const ushort GL_DONT_CARE = 0x1100;
    const ushort GL_FASTEST = 0x1101;
    const ushort GL_NICEST = 0x1102;

    /* Scissor box */
    const ushort GL_SCISSOR_BOX = 0x0C10;
    const ushort GL_SCISSOR_TEST = 0x0C11;

    /* Pixel Mode / Transfer */
    const ushort GL_MAP_COLOR = 0x0D10;
    const ushort GL_MAP_STENCIL = 0x0D11;
    const ushort GL_INDEX_SHIFT = 0x0D12;
    const ushort GL_INDEX_OFFSET = 0x0D13;
    const ushort GL_RED_SCALE = 0x0D14;
    const ushort GL_RED_BIAS = 0x0D15;
    const ushort GL_GREEN_SCALE = 0x0D18;
    const ushort GL_GREEN_BIAS = 0x0D19;
    const ushort GL_BLUE_SCALE = 0x0D1A;
    const ushort GL_BLUE_BIAS = 0x0D1B;
    const ushort GL_ALPHA_SCALE = 0x0D1C;
    const ushort GL_ALPHA_BIAS = 0x0D1D;
    const ushort GL_DEPTH_SCALE = 0x0D1E;
    const ushort GL_DEPTH_BIAS = 0x0D1F;
    const ushort GL_PIXEL_MAP_S_TO_S_SIZE = 0x0CB1;
    const ushort GL_PIXEL_MAP_I_TO_I_SIZE = 0x0CB0;
    const ushort GL_PIXEL_MAP_I_TO_R_SIZE = 0x0CB2;
    const ushort GL_PIXEL_MAP_I_TO_G_SIZE = 0x0CB3;
    const ushort GL_PIXEL_MAP_I_TO_B_SIZE = 0x0CB4;
    const ushort GL_PIXEL_MAP_I_TO_A_SIZE = 0x0CB5;
    const ushort GL_PIXEL_MAP_R_TO_R_SIZE = 0x0CB6;
    const ushort GL_PIXEL_MAP_G_TO_G_SIZE = 0x0CB7;
    const ushort GL_PIXEL_MAP_B_TO_B_SIZE = 0x0CB8;
    const ushort GL_PIXEL_MAP_A_TO_A_SIZE = 0x0CB9;
    const ushort GL_PIXEL_MAP_S_TO_S = 0x0C71;
    const ushort GL_PIXEL_MAP_I_TO_I = 0x0C70;
    const ushort GL_PIXEL_MAP_I_TO_R = 0x0C72;
    const ushort GL_PIXEL_MAP_I_TO_G = 0x0C73;
    const ushort GL_PIXEL_MAP_I_TO_B = 0x0C74;
    const ushort GL_PIXEL_MAP_I_TO_A = 0x0C75;
    const ushort GL_PIXEL_MAP_R_TO_R = 0x0C76;
    const ushort GL_PIXEL_MAP_G_TO_G = 0x0C77;
    const ushort GL_PIXEL_MAP_B_TO_B = 0x0C78;
    const ushort GL_PIXEL_MAP_A_TO_A = 0x0C79;
    const ushort GL_PACK_ALIGNMENT = 0x0D05;
    const ushort GL_PACK_LSB_FIRST = 0x0D01;
    const ushort GL_PACK_ROW_LENGTH = 0x0D02;
    const ushort GL_PACK_SKIP_PIXELS = 0x0D04;
    const ushort GL_PACK_SKIP_ROWS = 0x0D03;
    const ushort GL_PACK_SWAP_BYTES = 0x0D00;
    const ushort GL_UNPACK_ALIGNMENT = 0x0CF5;
    const ushort GL_UNPACK_LSB_FIRST = 0x0CF1;
    const ushort GL_UNPACK_ROW_LENGTH = 0x0CF2;
    const ushort GL_UNPACK_SKIP_PIXELS = 0x0CF4;
    const ushort GL_UNPACK_SKIP_ROWS = 0x0CF3;
    const ushort GL_UNPACK_SWAP_BYTES = 0x0CF0;
    const ushort GL_ZOOM_X = 0x0D16;
    const ushort GL_ZOOM_Y = 0x0D17;

    /* Texture mapping */
    const ushort GL_TEXTURE_ENV = 0x2300;
    const ushort GL_TEXTURE_ENV_MODE = 0x2200;
    const ushort GL_TEXTURE_1D = 0x0DE0;
    const ushort GL_TEXTURE_2D = 0x0DE1;
    const ushort GL_TEXTURE_WRAP_S = 0x2802;
    const ushort GL_TEXTURE_WRAP_T = 0x2803;
    const ushort GL_TEXTURE_MAG_FILTER = 0x2800;
    const ushort GL_TEXTURE_MIN_FILTER = 0x2801;
    const ushort GL_TEXTURE_ENV_COLOR = 0x2201;
    const ushort GL_TEXTURE_GEN_S = 0x0C60;
    const ushort GL_TEXTURE_GEN_T = 0x0C61;
    const ushort GL_TEXTURE_GEN_R = 0x0C62;
    const ushort GL_TEXTURE_GEN_Q = 0x0C63;
    const ushort GL_TEXTURE_GEN_MODE = 0x2500;
    const ushort GL_TEXTURE_BORDER_COLOR = 0x1004;
    const ushort GL_TEXTURE_WIDTH = 0x1000;
    const ushort GL_TEXTURE_HEIGHT = 0x1001;
    const ushort GL_TEXTURE_BORDER = 0x1005;
    const ushort GL_TEXTURE_COMPONENTS = 0x1003;
    const ushort GL_TEXTURE_RED_SIZE = 0x805C;
    const ushort GL_TEXTURE_GREEN_SIZE = 0x805D;
    const ushort GL_TEXTURE_BLUE_SIZE = 0x805E;
    const ushort GL_TEXTURE_ALPHA_SIZE = 0x805F;
    const ushort GL_TEXTURE_LUMINANCE_SIZE = 0x8060;
    const ushort GL_TEXTURE_INTENSITY_SIZE = 0x8061;
    const ushort GL_NEAREST_MIPMAP_NEAREST = 0x2700;
    const ushort GL_NEAREST_MIPMAP_LINEAR = 0x2702;
    const ushort GL_LINEAR_MIPMAP_NEAREST = 0x2701;
    const ushort GL_LINEAR_MIPMAP_LINEAR = 0x2703;
    const ushort GL_OBJECT_LINEAR = 0x2401;
    const ushort GL_OBJECT_PLANE = 0x2501;
    const ushort GL_EYE_LINEAR = 0x2400;
    const ushort GL_EYE_PLANE = 0x2502;
    const ushort GL_SPHERE_MAP = 0x2402;
    const ushort GL_DECAL = 0x2101;
    const ushort GL_MODULATE = 0x2100;
    const ushort GL_NEAREST = 0x2600;
    const ushort GL_REPEAT = 0x2901;
    const ushort GL_CLAMP = 0x2900;
    const ushort GL_S = 0x2000;
    const ushort GL_T = 0x2001;
    const ushort GL_R = 0x2002;
    const ushort GL_Q = 0x2003;

    /* Utility */
    const ushort GL_VENDOR = 0x1F00;
    const ushort GL_RENDERER = 0x1F01;
    const ushort GL_VERSION = 0x1F02;
    const ushort GL_EXTENSIONS = 0x1F03;

    /* Errors */
    const ushort GL_NO_ERROR = 0x0000;
    const ushort GL_INVALID_ENUM = 0x0500;
    const ushort GL_INVALID_VALUE = 0x0501;
    const ushort GL_INVALID_OPERATION = 0x0502;
    const ushort GL_STACK_OVERFLOW = 0x0503;
    const ushort GL_STACK_UNDERFLOW = 0x0504;
    const ushort GL_OUT_OF_MEMORY = 0x0505;

    /* glPush/PopAttrib bits */
    const uint GL_CURRENT_BIT = 0x00000001;
    const uint GL_POINT_BIT = 0x00000002;
    const uint GL_LINE_BIT = 0x00000004;
    const uint GL_POLYGON_BIT = 0x00000008;
    const uint GL_POLYGON_STIPPLE_BIT = 0x00000010;
    const uint GL_PIXEL_MODE_BIT = 0x00000020;
    const uint GL_LIGHTING_BIT = 0x00000040;
    const uint GL_FOG_BIT = 0x00000080;
    const uint GL_DEPTH_BUFFER_BIT = 0x00000100;
    const uint GL_ACCUM_BUFFER_BIT = 0x00000200;
    const uint GL_STENCIL_BUFFER_BIT = 0x00000400;
    const uint GL_VIEWPORT_BIT = 0x00000800;
    const uint GL_TRANSFORM_BIT = 0x00001000;
    const uint GL_ENABLE_BIT = 0x00002000;
    const uint GL_COLOR_BUFFER_BIT = 0x00004000;
    const uint GL_HINT_BIT = 0x00008000;
    const uint GL_EVAL_BIT = 0x00010000;
    const uint GL_LIST_BIT = 0x00020000;
    const uint GL_TEXTURE_BIT = 0x00040000;
    const uint GL_SCISSOR_BIT = 0x00080000;
    const uint GL_ALL_ATTRIB_BITS = 0x000FFFFF;


    /* OpenGL 1.1 */
    const ushort GL_PROXY_TEXTURE_1D = 0x8063;
    const ushort GL_PROXY_TEXTURE_2D = 0x8064;
    const ushort GL_TEXTURE_PRIORITY = 0x8066;
    const ushort GL_TEXTURE_RESIDENT = 0x8067;
    const ushort GL_TEXTURE_BINDING_1D = 0x8068;
    const ushort GL_TEXTURE_BINDING_2D = 0x8069;
    const ushort GL_TEXTURE_INTERNAL_FORMAT = 0x1003;
    const ushort GL_ALPHA4 = 0x803B;
    const ushort GL_ALPHA8 = 0x803C;
    const ushort GL_ALPHA12 = 0x803D;
    const ushort GL_ALPHA16 = 0x803E;
    const ushort GL_LUMINANCE4 = 0x803F;
    const ushort GL_LUMINANCE8 = 0x8040;
    const ushort GL_LUMINANCE12 = 0x8041;
    const ushort GL_LUMINANCE16 = 0x8042;
    const ushort GL_LUMINANCE4_ALPHA4 = 0x8043;
    const ushort GL_LUMINANCE6_ALPHA2 = 0x8044;
    const ushort GL_LUMINANCE8_ALPHA8 = 0x8045;
    const ushort GL_LUMINANCE12_ALPHA4 = 0x8046;
    const ushort GL_LUMINANCE12_ALPHA12 = 0x8047;
    const ushort GL_LUMINANCE16_ALPHA16 = 0x8048;
    const ushort GL_INTENSITY = 0x8049;
    const ushort GL_INTENSITY4 = 0x804A;
    const ushort GL_INTENSITY8 = 0x804B;
    const ushort GL_INTENSITY12 = 0x804C;
    const ushort GL_INTENSITY16 = 0x804D;
    const ushort GL_R3_G3_B2 = 0x2A10;
    const ushort GL_RGB4 = 0x804F;
    const ushort GL_RGB5 = 0x8050;
    const ushort GL_RGB8 = 0x8051;
    const ushort GL_RGB10 = 0x8052;
    const ushort GL_RGB12 = 0x8053;
    const ushort GL_RGB16 = 0x8054;
    const ushort GL_RGBA2 = 0x8055;
    const ushort GL_RGBA4 = 0x8056;
    const ushort GL_RGB5_A1 = 0x8057;
    const ushort GL_RGBA8 = 0x8058;
    const ushort GL_RGB10_A2 = 0x8059;
    const ushort GL_RGBA12 = 0x805A;
    const ushort GL_RGBA16 = 0x805B;
    const ushort GL_CLIENT_PIXEL_STORE_BIT = 0x0000;
    const ushort GL_CLIENT_VERTEX_ARRAY_BIT = 0x0000;
    const ushort GL_ALL_CLIENT_ATTRIB_BITS = 0xFFFF;
    const ushort GL_CLIENT_ALL_ATTRIB_BITS = 0xFFFF;

    const ushort GL_TEXTURE0_ARB = 0x84C0;
    const ushort GL_TEXTURE1_ARB = 0x84C1;
    const ushort GL_TEXTURE2_ARB = 0x84C2;
    const ushort GL_TEXTURE3_ARB = 0x84C3;
    const ushort GL_TEXTURE4_ARB = 0x84C4;
    const ushort GL_TEXTURE5_ARB = 0x84C5;
    const ushort GL_TEXTURE6_ARB = 0x84C6;
    const ushort GL_TEXTURE7_ARB = 0x84C7;
    const ushort GL_TEXTURE8_ARB = 0x84C8;
    const ushort GL_TEXTURE9_ARB = 0x84C9;
    const ushort GL_TEXTURE10_ARB = 0x84CA;
    const ushort GL_TEXTURE11_ARB = 0x84CB;
    const ushort GL_TEXTURE12_ARB = 0x84CC;
    const ushort GL_TEXTURE13_ARB = 0x84CD;
    const ushort GL_TEXTURE14_ARB = 0x84CE;
    const ushort GL_TEXTURE15_ARB = 0x84CF;
    const ushort GL_TEXTURE16_ARB = 0x84D0;
    const ushort GL_TEXTURE17_ARB = 0x84D1;
    const ushort GL_TEXTURE18_ARB = 0x84D2;
    const ushort GL_TEXTURE19_ARB = 0x84D3;
    const ushort GL_TEXTURE20_ARB = 0x84D4;
    const ushort GL_TEXTURE21_ARB = 0x84D5;
    const ushort GL_TEXTURE22_ARB = 0x84D6;
    const ushort GL_TEXTURE23_ARB = 0x84D7;
    const ushort GL_TEXTURE24_ARB = 0x84D8;
    const ushort GL_TEXTURE25_ARB = 0x84D9;
    const ushort GL_TEXTURE26_ARB = 0x84DA;
    const ushort GL_TEXTURE27_ARB = 0x84DB;
    const ushort GL_TEXTURE28_ARB = 0x84DC;
    const ushort GL_TEXTURE29_ARB = 0x84DD;
    const ushort GL_TEXTURE30_ARB = 0x84DE;
    const ushort GL_TEXTURE31_ARB = 0x84DF;
    const ushort GL_ACTIVE_TEXTURE_ARB = 0x84E0;
    const ushort GL_CLIENT_ACTIVE_TEXTURE_ARB = 0x84E1;
    const ushort GL_MAX_TEXTURE_UNITS_ARB = 0x84E2;

    const ushort GL_DEPTH_STENCIL_MESA = 0x8750;
    const ushort GL_UNSIGNED_INT_24_8_MESA = 0x8751;
    const ushort GL_UNSIGNED_INT_8_24_REV_MESA = 0x8752;
    const ushort GL_UNSIGNED_SHORT_15_1_MESA = 0x8753;
    const ushort GL_UNSIGNED_SHORT_1_15_REV_MESA = 0x8754;

    const ushort GL_TEXTURE_1D_ARRAY_EXT = 0x8C18;
    const ushort GL_PROXY_TEXTURE_1D_ARRAY_EXT = 0x8C19;
    const ushort GL_TEXTURE_2D_ARRAY_EXT = 0x8C1A;
    const ushort GL_PROXY_TEXTURE_2D_ARRAY_EXT = 0x8C1B;
    const ushort GL_TEXTURE_BINDING_1D_ARRAY_EXT = 0x8C1C;
    const ushort GL_TEXTURE_BINDING_2D_ARRAY_EXT = 0x8C1D;
    const ushort GL_MAX_ARRAY_TEXTURE_LAYERS_EXT = 0x88FF;
    const ushort GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER_EXT = 0x8CD4;

    const ushort GL_DEBUG_OBJECT_MESA = 0x8759;
    const ushort GL_DEBUG_PRINT_MESA = 0x875A;
    const ushort GL_DEBUG_ASSERT_MESA = 0x875B;

    const ushort GL_RESCALE_NORMAL = 0x803A;
    const ushort GL_CLAMP_TO_EDGE = 0x812F;
    const ushort GL_MAX_ELEMENTS_VERTICES = 0x80E8;
    const ushort GL_MAX_ELEMENTS_INDICES = 0x80E9;
    const ushort GL_BGR = 0x80E0;
    const ushort GL_BGRA = 0x80E1;
    const ushort GL_UNSIGNED_BYTE_3_3_2 = 0x8032;
    const ushort GL_UNSIGNED_BYTE_2_3_3_REV = 0x8362;
    const ushort GL_UNSIGNED_SHORT_5_6_5 = 0x8363;
    const ushort GL_UNSIGNED_SHORT_5_6_5_REV = 0x8364;
    const ushort GL_UNSIGNED_SHORT_4_4_4_4 = 0x8033;
    const ushort GL_UNSIGNED_SHORT_4_4_4_4_REV = 0x8365;
    const ushort GL_UNSIGNED_SHORT_5_5_5_1 = 0x8034;
    const ushort GL_UNSIGNED_SHORT_1_5_5_5_REV = 0x8366;
    const ushort GL_UNSIGNED_INT_8_8_8_8 = 0x8035;
    const ushort GL_UNSIGNED_INT_8_8_8_8_REV = 0x8367;
    const ushort GL_UNSIGNED_INT_10_10_10_2 = 0x8036;
    const ushort GL_UNSIGNED_INT_2_10_10_10_REV = 0x8368;
    const ushort GL_LIGHT_MODEL_COLOR_CONTROL = 0x81F8;
    const ushort GL_SINGLE_COLOR = 0x81F9;
    const ushort GL_SEPARATE_SPECULAR_COLOR = 0x81FA;
    const ushort GL_TEXTURE_MIN_LOD = 0x813A;
    const ushort GL_TEXTURE_MAX_LOD = 0x813B;
    const ushort GL_TEXTURE_BASE_LEVEL = 0x813C;
    const ushort GL_TEXTURE_MAX_LEVEL = 0x813D;
    const ushort GL_SMOOTH_POINT_SIZE_RANGE = 0x0B12;
    const ushort GL_SMOOTH_POINT_SIZE_GRANULARITY = 0x0B13;
    const ushort GL_SMOOTH_LINE_WIDTH_RANGE = 0x0B22;
    const ushort GL_SMOOTH_LINE_WIDTH_GRANULARITY = 0x0B23;
    const ushort GL_ALIASED_POINT_SIZE_RANGE = 0x846D;
    const ushort GL_ALIASED_LINE_WIDTH_RANGE = 0x846E;
    const ushort GL_PACK_SKIP_IMAGES = 0x806B;
    const ushort GL_PACK_IMAGE_HEIGHT = 0x806C;
    const ushort GL_UNPACK_SKIP_IMAGES = 0x806D;
    const ushort GL_UNPACK_IMAGE_HEIGHT = 0x806E;
    const ushort GL_TEXTURE_3D = 0x806F;
    const ushort GL_PROXY_TEXTURE_3D = 0x8070;
    const ushort GL_TEXTURE_DEPTH = 0x8071;
    const ushort GL_TEXTURE_WRAP_R = 0x8072;
    const ushort GL_MAX_3D_TEXTURE_SIZE = 0x8073;
    const ushort GL_TEXTURE_BINDING_3D = 0x806A;

    const ushort GL_CONSTANT_COLOR = 0x8001;
    const ushort GL_ONE_MINUS_CONSTANT_COLOR = 0x8002;
    const ushort GL_CONSTANT_ALPHA = 0x8003;
    const ushort GL_ONE_MINUS_CONSTANT_ALPHA = 0x8004;
    const ushort GL_COLOR_TABLE = 0x80D0;
    const ushort GL_POST_CONVOLUTION_COLOR_TABLE = 0x80D1;
    const ushort GL_POST_COLOR_MATRIX_COLOR_TABLE = 0x80D2;
    const ushort GL_PROXY_COLOR_TABLE = 0x80D3;
    const ushort GL_PROXY_POST_CONVOLUTION_COLOR_TABLE = 0x80D4;
    const ushort GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE = 0x80D5;
    const ushort GL_COLOR_TABLE_SCALE = 0x80D6;
    const ushort GL_COLOR_TABLE_BIAS = 0x80D7;
    const ushort GL_COLOR_TABLE_FORMAT = 0x80D8;
    const ushort GL_COLOR_TABLE_WIDTH = 0x80D9;
    const ushort GL_COLOR_TABLE_RED_SIZE = 0x80DA;
    const ushort GL_COLOR_TABLE_GREEN_SIZE = 0x80DB;
    const ushort GL_COLOR_TABLE_BLUE_SIZE = 0x80DC;
    const ushort GL_COLOR_TABLE_ALPHA_SIZE = 0x80DD;
    const ushort GL_COLOR_TABLE_LUMINANCE_SIZE = 0x80DE;
    const ushort GL_COLOR_TABLE_INTENSITY_SIZE = 0x80DF;
    const ushort GL_CONVOLUTION_1D = 0x8010;
    const ushort GL_CONVOLUTION_2D = 0x8011;
    const ushort GL_SEPARABLE_2D = 0x8012;
    const ushort GL_CONVOLUTION_BORDER_MODE = 0x8013;
    const ushort GL_CONVOLUTION_FILTER_SCALE = 0x8014;
    const ushort GL_CONVOLUTION_FILTER_BIAS = 0x8015;
    const ushort GL_REDUCE = 0x8016;
    const ushort GL_CONVOLUTION_FORMAT = 0x8017;
    const ushort GL_CONVOLUTION_WIDTH = 0x8018;
    const ushort GL_CONVOLUTION_HEIGHT = 0x8019;
    const ushort GL_MAX_CONVOLUTION_WIDTH = 0x801A;
    const ushort GL_MAX_CONVOLUTION_HEIGHT = 0x801B;
    const ushort GL_POST_CONVOLUTION_RED_SCALE = 0x801C;
    const ushort GL_POST_CONVOLUTION_GREEN_SCALE = 0x801D;
    const ushort GL_POST_CONVOLUTION_BLUE_SCALE = 0x801E;
    const ushort GL_POST_CONVOLUTION_ALPHA_SCALE = 0x801F;
    const ushort GL_POST_CONVOLUTION_RED_BIAS = 0x8020;
    const ushort GL_POST_CONVOLUTION_GREEN_BIAS = 0x8021;
    const ushort GL_POST_CONVOLUTION_BLUE_BIAS = 0x8022;
    const ushort GL_POST_CONVOLUTION_ALPHA_BIAS = 0x8023;
    const ushort GL_CONSTANT_BORDER = 0x8151;
    const ushort GL_REPLICATE_BORDER = 0x8153;
    const ushort GL_CONVOLUTION_BORDER_COLOR = 0x8154;
    const ushort GL_COLOR_MATRIX = 0x80B1;
    const ushort GL_COLOR_MATRIX_STACK_DEPTH = 0x80B2;
    const ushort GL_MAX_COLOR_MATRIX_STACK_DEPTH = 0x80B3;
    const ushort GL_POST_COLOR_MATRIX_RED_SCALE = 0x80B4;
    const ushort GL_POST_COLOR_MATRIX_GREEN_SCALE = 0x80B5;
    const ushort GL_POST_COLOR_MATRIX_BLUE_SCALE = 0x80B6;
    const ushort GL_POST_COLOR_MATRIX_ALPHA_SCALE = 0x80B7;
    const ushort GL_POST_COLOR_MATRIX_RED_BIAS = 0x80B8;
    const ushort GL_POST_COLOR_MATRIX_GREEN_BIAS = 0x80B9;
    const ushort GL_POST_COLOR_MATRIX_BLUE_BIAS = 0x80BA;
    const ushort GL_POST_COLOR_MATRIX_ALPHA_BIAS = 0x80BB;
    const ushort GL_HISTOGRAM = 0x8024;
    const ushort GL_PROXY_HISTOGRAM = 0x8025;
    const ushort GL_HISTOGRAM_WIDTH = 0x8026;
    const ushort GL_HISTOGRAM_FORMAT = 0x8027;
    const ushort GL_HISTOGRAM_RED_SIZE = 0x8028;
    const ushort GL_HISTOGRAM_GREEN_SIZE = 0x8029;
    const ushort GL_HISTOGRAM_BLUE_SIZE = 0x802A;
    const ushort GL_HISTOGRAM_ALPHA_SIZE = 0x802B;
    const ushort GL_HISTOGRAM_LUMINANCE_SIZE = 0x802C;
    const ushort GL_HISTOGRAM_SINK = 0x802D;
    const ushort GL_MINMAX = 0x802E;
    const ushort GL_MINMAX_FORMAT = 0x802F;
    const ushort GL_MINMAX_SINK = 0x8030;
    const ushort GL_TABLE_TOO_LARGE = 0x8031;
    const ushort GL_BLEND_EQUATION = 0x8009;
    const ushort GL_MIN = 0x8007;
    const ushort GL_MAX = 0x8008;
    const ushort GL_FUNC_ADD = 0x8006;
    const ushort GL_FUNC_SUBTRACT = 0x800A;
    const ushort GL_FUNC_REVERSE_SUBTRACT = 0x800B;
    const ushort GL_BLEND_COLOR = 0x8005;

    /* multitexture */
    const ushort GL_TEXTURE0 = 0x84C0;
    const ushort GL_TEXTURE1 = 0x84C1;
    const ushort GL_TEXTURE2 = 0x84C2;
    const ushort GL_TEXTURE3 = 0x84C3;
    const ushort GL_TEXTURE4 = 0x84C4;
    const ushort GL_TEXTURE5 = 0x84C5;
    const ushort GL_TEXTURE6 = 0x84C6;
    const ushort GL_TEXTURE7 = 0x84C7;
    const ushort GL_TEXTURE8 = 0x84C8;
    const ushort GL_TEXTURE9 = 0x84C9;
    const ushort GL_TEXTURE10 = 0x84CA;
    const ushort GL_TEXTURE11 = 0x84CB;
    const ushort GL_TEXTURE12 = 0x84CC;
    const ushort GL_TEXTURE13 = 0x84CD;
    const ushort GL_TEXTURE14 = 0x84CE;
    const ushort GL_TEXTURE15 = 0x84CF;
    const ushort GL_TEXTURE16 = 0x84D0;
    const ushort GL_TEXTURE17 = 0x84D1;
    const ushort GL_TEXTURE18 = 0x84D2;
    const ushort GL_TEXTURE19 = 0x84D3;
    const ushort GL_TEXTURE20 = 0x84D4;
    const ushort GL_TEXTURE21 = 0x84D5;
    const ushort GL_TEXTURE22 = 0x84D6;
    const ushort GL_TEXTURE23 = 0x84D7;
    const ushort GL_TEXTURE24 = 0x84D8;
    const ushort GL_TEXTURE25 = 0x84D9;
    const ushort GL_TEXTURE26 = 0x84DA;
    const ushort GL_TEXTURE27 = 0x84DB;
    const ushort GL_TEXTURE28 = 0x84DC;
    const ushort GL_TEXTURE29 = 0x84DD;
    const ushort GL_TEXTURE30 = 0x84DE;
    const ushort GL_TEXTURE31 = 0x84DF;
    const ushort GL_ACTIVE_TEXTURE = 0x84E0;
    const ushort GL_CLIENT_ACTIVE_TEXTURE = 0x84E1;
    const ushort GL_MAX_TEXTURE_UNITS = 0x84E2;
    /* texture_cube_map */
    const ushort GL_NORMAL_MAP = 0x8511;
    const ushort GL_REFLECTION_MAP = 0x8512;
    const ushort GL_TEXTURE_CUBE_MAP = 0x8513;
    const ushort GL_TEXTURE_BINDING_CUBE_MAP = 0x8514;
    const ushort GL_TEXTURE_CUBE_MAP_POSITIVE_X = 0x8515;
    const ushort GL_TEXTURE_CUBE_MAP_NEGATIVE_X = 0x8516;
    const ushort GL_TEXTURE_CUBE_MAP_POSITIVE_Y = 0x8517;
    const ushort GL_TEXTURE_CUBE_MAP_NEGATIVE_Y = 0x8518;
    const ushort GL_TEXTURE_CUBE_MAP_POSITIVE_Z = 0x8519;
    const ushort GL_TEXTURE_CUBE_MAP_NEGATIVE_Z = 0x851A;
    const ushort GL_PROXY_TEXTURE_CUBE_MAP = 0x851B;
    const ushort GL_MAX_CUBE_MAP_TEXTURE_SIZE = 0x851C;
    /* texture_compression */
    const ushort GL_COMPRESSED_ALPHA = 0x84E9;
    const ushort GL_COMPRESSED_LUMINANCE = 0x84EA;
    const ushort GL_COMPRESSED_LUMINANCE_ALPHA = 0x84EB;
    const ushort GL_COMPRESSED_INTENSITY = 0x84EC;
    const ushort GL_COMPRESSED_RGB = 0x84ED;
    const ushort GL_COMPRESSED_RGBA = 0x84EE;
    const ushort GL_TEXTURE_COMPRESSION_HINT = 0x84EF;
    const ushort GL_TEXTURE_COMPRESSED_IMAGE_SIZE = 0x86A0;
    const ushort GL_TEXTURE_COMPRESSED = 0x86A1;
    const ushort GL_NUM_COMPRESSED_TEXTURE_FORMATS = 0x86A2;
    const ushort GL_COMPRESSED_TEXTURE_FORMATS = 0x86A3;
    /* multisample */
    const ushort GL_MULTISAMPLE = 0x809D;
    const ushort GL_SAMPLE_ALPHA_TO_COVERAGE = 0x809E;
    const ushort GL_SAMPLE_ALPHA_TO_ONE = 0x809F;
    const ushort GL_SAMPLE_COVERAGE = 0x80A0;
    const ushort GL_SAMPLE_BUFFERS = 0x80A8;
    const ushort GL_SAMPLES = 0x80A9;
    const ushort GL_SAMPLE_COVERAGE_VALUE = 0x80AA;
    const ushort GL_SAMPLE_COVERAGE_INVERT = 0x80AB;
    const ushort GL_MULTISAMPLE_BIT = 0x2000;
    /* transpose_matrix */
    const ushort GL_TRANSPOSE_MODELVIEW_MATRIX = 0x84E3;
    const ushort GL_TRANSPOSE_PROJECTION_MATRIX = 0x84E4;
    const ushort GL_TRANSPOSE_TEXTURE_MATRIX = 0x84E5;
    const ushort GL_TRANSPOSE_COLOR_MATRIX = 0x84E6;
    /* texture_env_combine */
    const ushort GL_COMBINE = 0x8570;
    const ushort GL_COMBINE_RGB = 0x8571;
    const ushort GL_COMBINE_ALPHA = 0x8572;
    const ushort GL_SOURCE0_RGB = 0x8580;
    const ushort GL_SOURCE1_RGB = 0x8581;
    const ushort GL_SOURCE2_RGB = 0x8582;
    const ushort GL_SOURCE0_ALPHA = 0x8588;
    const ushort GL_SOURCE1_ALPHA = 0x8589;
    const ushort GL_SOURCE2_ALPHA = 0x858A;
    const ushort GL_OPERAND0_RGB = 0x8590;
    const ushort GL_OPERAND1_RGB = 0x8591;
    const ushort GL_OPERAND2_RGB = 0x8592;
    const ushort GL_OPERAND0_ALPHA = 0x8598;
    const ushort GL_OPERAND1_ALPHA = 0x8599;
    const ushort GL_OPERAND2_ALPHA = 0x859A;
    const ushort GL_RGB_SCALE = 0x8573;
    const ushort GL_ADD_SIGNED = 0x8574;
    const ushort GL_INTERPOLATE = 0x8575;
    const ushort GL_SUBTRACT = 0x84E7;
    const ushort GL_CONSTANT = 0x8576;
    const ushort GL_PRIMARY_COLOR = 0x8577;
    const ushort GL_PREVIOUS = 0x8578;
    /* texture_env_dot3 */
    const ushort GL_DOT3_RGB = 0x86AE;
    const ushort GL_DOT3_RGBA = 0x86AF;
    /* texture_border_clamp */
    const ushort GL_CLAMP_TO_BORDER = 0x812D;
}