extern (C) {

    //  Matches OpenGL's right now.
    //! Data formats \link Formats Formats\endlink
    const ushort IL_COLOUR_INDEX = 0x1900;
    const ushort IL_COLOR_INDEX = 0x1900;
    const ushort IL_ALPHA = 0x1906;
    const ushort IL_RGB = 0x1907;
    const ushort IL_RGBA = 0x1908;
    const ushort IL_BGR = 0x80E0;
    const ushort IL_BGRA = 0x80E1;
    const ushort IL_LUMINANCE = 0x1909;
    const ushort IL_LUMINANCE_ALPHA = 0x190A;

    //! Data types \link Types Types\endlink
    const ushort IL_BYTE = 0x1400;
    const ushort IL_UNSIGNED_BYTE = 0x1401;
    const ushort IL_SHORT = 0x1402;
    const ushort IL_UNSIGNED_SHORT = 0x1403;
    const ushort IL_INT = 0x1404;
    const ushort IL_UNSIGNED_INT = 0x1405;
    const ushort IL_FLOAT = 0x1406;
    const ushort IL_DOUBLE = 0x140A;
    const ushort IL_HALF = 0x140B;



    const ushort IL_VENDOR = 0x1F00;
    const ushort IL_LOAD_EXT = 0x1F01;
    const ushort IL_SAVE_EXT = 0x1F02;

    // Attribute Bits
    const uint IL_ORIGIN_BIT = 0x00000001;
    const uint IL_FILE_BIT = 0x00000002;
    const uint IL_PAL_BIT = 0x00000004;
    const uint IL_FORMAT_BIT = 0x00000008;
    const uint IL_TYPE_BIT = 0x00000010;
    const uint IL_COMPRESS_BIT = 0x00000020;
    const uint IL_LOADFAIL_BIT = 0x00000040;
    const uint IL_FORMAT_SPECIFIC_BIT = 0x00000080;
    const uint IL_ALL_ATTRIB_BITS = 0x000FFFFF;


    // Palette types
    const ushort IL_PAL_NONE = 0x0400;
    const ushort IL_PAL_RGB24 = 0x0401;
    const ushort IL_PAL_RGB32 = 0x0402;
    const ushort IL_PAL_RGBA32 = 0x0403;
    const ushort IL_PAL_BGR24 = 0x0404;
    const ushort IL_PAL_BGR32 = 0x0405;
    const ushort IL_PAL_BGRA32 = 0x0406;


    // Image types
    const ushort IL_TYPE_UNKNOWN = 0x0000;
    const ushort IL_BMP = 0x0420;  //!< Microsoft Windows Bitmap - .bmp extension
    const ushort IL_CUT = 0x0421;  //!< Dr. Halo - .cut extension
    const ushort IL_DOOM = 0x0422;  //!< DooM walls - no specific extension
    const ushort IL_DOOM_FLAT = 0x0423;  //!< DooM flats - no specific extension
    const ushort IL_ICO = 0x0424;  //!< Microsoft Windows Icons and Cursors - .ico and .cur extensions
    const ushort IL_JPG = 0x0425;  //!< JPEG - .jpg, .jpe and .jpeg extensions
    const ushort IL_JFIF = 0x0425;
    const ushort IL_ILBM = 0x0426;  //!< Amiga IFF (FORM ILBM) - .iff, .ilbm, .lbm extensions
    const ushort IL_PCD = 0x0427;  //!< Kodak PhotoCD - .pcd extension
    const ushort IL_PCX = 0x0428;  //!< ZSoft PCX - .pcx extension
    const ushort IL_PIC = 0x0429;  //!< PIC - .pic extension
    const ushort IL_PNG = 0x042A;
    const ushort IL_PNM = 0x042B;  //!< Portable Any Map - .pbm, .pgm, .ppm and .pnm extensions
    const ushort IL_SGI = 0x042C;
    const ushort IL_TGA = 0x042D;  //!< TrueVision Targa File - .tga, .vda, .icb and .vst extensions
    const ushort IL_TIF = 0x042E;  //!< Tagged Image File Format - .tif and .tiff extensions
    const ushort IL_CHEAD = 0x042F;  //!< C-Style Header - .h extension
    const ushort IL_RAW = 0x0430;  //!< Raw Image Data - any extension
    const ushort IL_MDL = 0x0431;
    const ushort IL_WAL = 0x0432;  //!< Quake 2 Texture - .wal extension
    const ushort IL_LIF = 0x0434;
    const ushort IL_MNG = 0x0435;
    const ushort IL_JNG = 0x0435;
    const ushort IL_GIF = 0x0436;  //!< Graphics Interchange Format - .gif extension
    const ushort IL_DDS = 0x0437;  //!< DirectDraw Surface - .dds extension
    const ushort IL_DCX = 0x0438;  //!< ZSoft Multi-PCX - .dcx extension
    const ushort IL_PSD = 0x0439;  //!< Adobe PhotoShop - .psd extension
    const ushort IL_EXIF = 0x043A;
    const ushort IL_PSP = 0x043B;
    const ushort IL_PIX = 0x043C;
    const ushort IL_PXR = 0x043D;
    const ushort IL_XPM = 0x043E;
    const ushort IL_HDR = 0x043F;  //!< Radiance High Dynamic Range - .hdr extension
    const ushort IL_ICNS = 0x0440;
    const ushort IL_JP2 = 0x0441;  //!< Jpeg 2000 - .jp2 extension
    const ushort IL_EXR = 0x0442;  //!< OpenEXR - .exr extension
    const ushort IL_WDP = 0x0443;  //!< Microsoft HD Photo - .wdp and .hdp extension
    const ushort IL_VTF = 0x0444;  //!< Valve Texture Format - .vtf extension
    const ushort IL_WBMP = 0x0445;  //!< Wireless Bitmap - .wbmp extension
    const ushort IL_SUN = 0x0446;  //!< Sun Raster - .sun, .ras, .rs, .im1, .im8, .im24 and .im32 extensions
    const ushort IL_IFF = 0x0447;  //!< Interchange File Format - .iff extension
    const ushort IL_TPL = 0x0448;
    const ushort IL_FITS = 0x0449;  //!< Flexible Image Transport System - .fit and .fits extensions
    const ushort IL_DICOM = 0x044A;  //!< Digital Imaging and Communications in Medicine (DICOM) - .dcm and .dicom extensions
    const ushort IL_IWI = 0x044B;  //!< Call of Duty Infinity Ward Image - .iwi extension
    const ushort IL_BLP = 0x044C;  //!< Blizzard Texture Format - .blp extension
    const ushort IL_FTX = 0x044D;  //!< Heavy Metal: FAKK2 Texture - .ftx extension
    const ushort IL_ROT = 0x044E;  //!< Homeworld 2 - Relic Texture - .rot extension
    const ushort IL_TEXTURE = 0x044F;
    const ushort IL_DPX = 0x0450;  //!< Digital Picture Exchange - .dpx extension
    const ushort IL_UTX = 0x0451;
    const ushort IL_MP3 = 0x0452;  //!< MPEG-1 Audio Layer 3 - .mp3 extension


    const ushort IL_JASC_PAL = 0x0475;


    // Error Types
    const ushort IL_NO_ERROR = 0x0000;
    const ushort IL_INVALID_ENUM = 0x0501;
    const ushort IL_OUT_OF_MEMORY = 0x0502;
    const ushort IL_FORMAT_NOT_SUPPORTED = 0x0503;
    const ushort IL_INTERNAL_ERROR = 0x0504;
    const ushort IL_INVALID_VALUE = 0x0505;
    const ushort IL_ILLEGAL_OPERATION = 0x0506;
    const ushort IL_ILLEGAL_FILE_VALUE = 0x0507;
    const ushort IL_INVALID_FILE_HEADER = 0x0508;
    const ushort IL_INVALID_PARAM = 0x0509;
    const ushort IL_COULD_NOT_OPEN_FILE = 0x050A;
    const ushort IL_INVALID_EXTENSION = 0x050B;
    const ushort IL_FILE_ALREADY_EXISTS = 0x050C;
    const ushort IL_OUT_FORMAT_SAME = 0x050D;
    const ushort IL_STACK_OVERFLOW = 0x050E;
    const ushort IL_STACK_UNDERFLOW = 0x050F;
    const ushort IL_INVALID_CONVERSION = 0x0510;
    const ushort IL_BAD_DIMENSIONS = 0x0511;
    const ushort IL_FILE_READ_ERROR = 0x0512;  // 05/12/2002: Addition by Sam.
    const ushort IL_FILE_WRITE_ERROR = 0x0512;

    const ushort IL_LIB_GIF_ERROR = 0x05E1;
    const ushort IL_LIB_JPEG_ERROR = 0x05E2;
    const ushort IL_LIB_PNG_ERROR = 0x05E3;
    const ushort IL_LIB_TIFF_ERROR = 0x05E4;
    const ushort IL_LIB_MNG_ERROR = 0x05E5;
    const ushort IL_LIB_JP2_ERROR = 0x05E6;
    const ushort IL_LIB_EXR_ERROR = 0x05E7;
    const ushort IL_UNKNOWN_ERROR = 0x05FF;


    // Origin Definitions
    const ushort IL_ORIGIN_SET = 0x0600;
    const ushort IL_ORIGIN_LOWER_LEFT = 0x0601;
    const ushort IL_ORIGIN_UPPER_LEFT = 0x0602;
    const ushort IL_ORIGIN_MODE = 0x0603;


    // Format and Type Mode Definitions
    const ushort IL_FORMAT_SET = 0x0610;
    const ushort IL_FORMAT_MODE = 0x0611;
    const ushort IL_TYPE_SET = 0x0612;
    const ushort IL_TYPE_MODE = 0x0613;


    // File definitions
    const ushort IL_FILE_OVERWRITE = 0x0620;
    const ushort IL_FILE_MODE = 0x0621;


    // Palette definitions
    const ushort IL_CONV_PAL = 0x0630;


    // Load fail definitions
    const ushort IL_DEFAULT_ON_FAIL = 0x0632;


    // Key colour and alpha definitions
    const ushort IL_USE_KEY_COLOUR = 0x0635;
    const ushort IL_USE_KEY_COLOR = 0x0635;
    const ushort IL_BLIT_BLEND = 0x0636;


    // Interlace definitions
    const ushort IL_SAVE_INTERLACED = 0x0639;
    const ushort IL_INTERLACE_MODE = 0x063A;


    // Quantization definitions
    const ushort IL_QUANTIZATION_MODE = 0x0640;
    const ushort IL_WU_QUANT = 0x0641;
    const ushort IL_NEU_QUANT = 0x0642;
    const ushort IL_NEU_QUANT_SAMPLE = 0x0643;
    const ushort IL_MAX_QUANT_INDEXS = 0x0644; //XIX : int : Maximum number of colors to reduce to, default of 256. and has a range of 2-256
    const ushort IL_MAX_QUANT_INDICES = 0x0644;


    // Hints
    const ushort IL_FASTEST = 0x0660;
    const ushort IL_LESS_MEM = 0x0661;
    const ushort IL_DONT_CARE = 0x0662;
    const ushort IL_MEM_SPEED_HINT = 0x0665;
    const ushort IL_USE_COMPRESSION = 0x0666;
    const ushort IL_NO_COMPRESSION = 0x0667;
    const ushort IL_COMPRESSION_HINT = 0x0668;


    // Compression
    const ushort IL_NVIDIA_COMPRESS = 0x0670;
    const ushort IL_SQUISH_COMPRESS = 0x0671;


    // Subimage types
    const ushort IL_SUB_NEXT = 0x0680;
    const ushort IL_SUB_MIPMAP = 0x0681;
    const ushort IL_SUB_LAYER = 0x0682;


    // Compression definitions
    const ushort IL_COMPRESS_MODE = 0x0700;
    const ushort IL_COMPRESS_NONE = 0x0701;
    const ushort IL_COMPRESS_RLE = 0x0702;
    const ushort IL_COMPRESS_LZO = 0x0703;
    const ushort IL_COMPRESS_ZLIB = 0x0704;


    // File format-specific values
    const ushort IL_TGA_CREATE_STAMP = 0x0710;
    const ushort IL_JPG_QUALITY = 0x0711;
    const ushort IL_PNG_INTERLACE = 0x0712;
    const ushort IL_TGA_RLE = 0x0713;
    const ushort IL_BMP_RLE = 0x0714;
    const ushort IL_SGI_RLE = 0x0715;
    const ushort IL_TGA_ID_STRING = 0x0717;
    const ushort IL_TGA_AUTHNAME_STRING = 0x0718;
    const ushort IL_TGA_AUTHCOMMENT_STRING = 0x0719;
    const ushort IL_PNG_AUTHNAME_STRING = 0x071A;
    const ushort IL_PNG_TITLE_STRING = 0x071B;
    const ushort IL_PNG_DESCRIPTION_STRING = 0x071C;
    const ushort IL_TIF_DESCRIPTION_STRING = 0x071D;
    const ushort IL_TIF_HOSTCOMPUTER_STRING = 0x071E;
    const ushort IL_TIF_DOCUMENTNAME_STRING = 0x071F;
    const ushort IL_TIF_AUTHNAME_STRING = 0x0720;
    const ushort IL_JPG_SAVE_FORMAT = 0x0721;
    const ushort IL_CHEAD_HEADER_STRING = 0x0722;
    const ushort IL_PCD_PICNUM = 0x0723;
    const ushort IL_PNG_ALPHA_INDEX = 0x0724; //XIX : int : the color in the palette at this index value (0-255) is considered transparent, -1 for no trasparent color
    const ushort IL_JPG_PROGRESSIVE = 0x0725;
    const ushort IL_VTF_COMP = 0x0726;


    // DXTC definitions
    const ushort IL_DXTC_FORMAT = 0x0705;
    const ushort IL_DXT1 = 0x0706;
    const ushort IL_DXT2 = 0x0707;
    const ushort IL_DXT3 = 0x0708;
    const ushort IL_DXT4 = 0x0709;
    const ushort IL_DXT5 = 0x070A;
    const ushort IL_DXT_NO_COMP = 0x070B;
    const ushort IL_KEEP_DXTC_DATA = 0x070C;
    const ushort IL_DXTC_DATA_FORMAT = 0x070D;
    const ushort IL_3DC = 0x070E;
    const ushort IL_RXGB = 0x070F;
    const ushort IL_ATI1N = 0x0710;
    const ushort IL_DXT1A = 0x0711;  // Normally the same as IL_DXT1, except for nVidia Texture Tools.

    // Environment map definitions
    const uint IL_CUBEMAP_POSITIVEX = 0x00000400;
    const uint IL_CUBEMAP_NEGATIVEX = 0x00000800;
    const uint IL_CUBEMAP_POSITIVEY = 0x00001000;
    const uint IL_CUBEMAP_NEGATIVEY = 0x00002000;
    const uint IL_CUBEMAP_POSITIVEZ = 0x00004000;
    const uint IL_CUBEMAP_NEGATIVEZ = 0x00008000;
    const uint IL_SPHEREMAP = 0x00010000;


    // Values
    const ushort IL_VERSION_NUM = 0x0DE2;
    const ushort IL_IMAGE_WIDTH = 0x0DE4;
    const ushort IL_IMAGE_HEIGHT = 0x0DE5;
    const ushort IL_IMAGE_DEPTH = 0x0DE6;
    const ushort IL_IMAGE_SIZE_OF_DATA = 0x0DE7;
    const ushort IL_IMAGE_BPP = 0x0DE8;
    const ushort IL_IMAGE_BYTES_PER_PIXEL = 0x0DE8;
    const ushort IL_IMAGE_BITS_PER_PIXEL = 0x0DE9;
    const ushort IL_IMAGE_FORMAT = 0x0DEA;
    const ushort IL_IMAGE_TYPE = 0x0DEB;
    const ushort IL_PALETTE_TYPE = 0x0DEC;
    const ushort IL_PALETTE_SIZE = 0x0DED;
    const ushort IL_PALETTE_BPP = 0x0DEE;
    const ushort IL_PALETTE_NUM_COLS = 0x0DEF;
    const ushort IL_PALETTE_BASE_TYPE = 0x0DF0;
    const ushort IL_NUM_FACES = 0x0DE1;
    const ushort IL_NUM_IMAGES = 0x0DF1;
    const ushort IL_NUM_MIPMAPS = 0x0DF2;
    const ushort IL_NUM_LAYERS = 0x0DF3;
    const ushort IL_ACTIVE_IMAGE = 0x0DF4;
    const ushort IL_ACTIVE_MIPMAP = 0x0DF5;
    const ushort IL_ACTIVE_LAYER = 0x0DF6;
    const ushort IL_ACTIVE_FACE = 0x0E00;
    const ushort IL_CUR_IMAGE = 0x0DF7;
    const ushort IL_IMAGE_DURATION = 0x0DF8;
    const ushort IL_IMAGE_PLANESIZE = 0x0DF9;
    const ushort IL_IMAGE_BPC = 0x0DFA;
    const ushort IL_IMAGE_OFFX = 0x0DFB;
    const ushort IL_IMAGE_OFFY = 0x0DFC;
    const ushort IL_IMAGE_CUBEFLAGS = 0x0DFD;
    const ushort IL_IMAGE_ORIGIN = 0x0DFE;
    const ushort IL_IMAGE_CHANNELS = 0x0DFF;

    // ImageLib Functions
    ubyte ilActiveFace(uint Number);
    ubyte ilActiveImage(uint Number);
    ubyte ilActiveLayer(uint Number);
    ubyte ilActiveMipmap(uint Number);
    ubyte ilApplyPal(char * FileName);
    void ilBindImage(uint Image);
    ubyte ilBlit(uint Source, int DestX, int DestY, int DestZ, uint SrcX, uint SrcY, uint SrcZ, uint Width, uint Height, uint Depth);
    ubyte ilClampNTSC();
    void ilClearColour(float Red, float Green, float Blue, float Alpha);
    ubyte ilClearImage();
    uint ilCloneCurImage();
    ubyte* ilCompressDXT(ubyte *Data, uint Width, uint Height, uint Depth, uint DXTCFormat, uint *DXTCSize);
    ubyte ilCompressFunc(uint Mode);
    ubyte ilConvertImage(uint DestFormat, uint DestType);
    ubyte ilConvertPal(uint DestFormat);
    ubyte ilCopyImage(uint Src);
    uint ilCopyPixels(uint XOff, uint YOff, uint ZOff, uint Width, uint Height, uint Depth, uint Format, uint Type, void *Data);
    uint ilCreateSubImage(uint Type, uint Num);
    ubyte ilDefaultImage();
    void ilDeleteImage(const uint Num);
    void ilDeleteImages(size_t Num, const uint *Images);
    uint ilDetermineType(char * FileName);
    uint ilDetermineTypeF(void * File);
    uint ilDetermineTypeL(const void *Lump, uint Size);
    ubyte ilDisable(uint Mode);
    ubyte ilDxtcDataToImage();
    ubyte ilDxtcDataToSurface();
    ubyte ilEnable(uint Mode);
    void ilFlipSurfaceDxtcData();
    ubyte ilFormatFunc(uint Mode);
    void ilGenImages(size_t Num, uint *Images);
    uint ilGenImage();
    ubyte* ilGetAlpha(uint Type);
    ubyte ilGetBoolean(uint Mode);
    void ilGetBooleanv(uint Mode, ubyte *Param);
    ubyte* ilGetData();
    uint ilGetDXTCData(void *Buffer, uint BufferSize, uint DXTCFormat);
    uint ilGetError();
    int ilGetInteger(uint Mode);
    void ilGetIntegerv(uint Mode, int *Param);
    uint ilGetLumpPos();
    ubyte* ilGetPalette();
    char * ilGetString(uint StringName);
    void ilHint(uint Target, uint Mode);
    ubyte ilInvertSurfaceDxtcDataAlpha();
    void ilInit();
    ubyte ilImageToDxtcData(uint Format);
    ubyte ilIsDisabled(uint Mode);
    ubyte ilIsEnabled(uint Mode);
    ubyte ilIsImage(uint Image);
    ubyte ilIsValid(uint Type, char * FileName);
    ubyte ilIsValidF(uint Type, void * File);
    ubyte ilIsValidL(uint Type, void *Lump, uint Size);
    void ilKeyColour(float Red, float Green, float Blue, float Alpha);
    ubyte ilLoad(uint Type, char * FileName);
    ubyte ilLoadF(uint Type, void * File);
    ubyte ilLoadImage(char * FileName);
    ubyte ilLoadL(uint Type, const void *Lump, uint Size);
    ubyte ilLoadPal(char * FileName);
    void ilModAlpha(double AlphaValue);
    ubyte ilOriginFunc(uint Mode);
    ubyte ilOverlayImage(uint Source, int XCoord, int YCoord, int ZCoord);
    void ilPopAttrib();
    void ilPushAttrib(uint Bits);
    void ilRegisterFormat(uint Format);
    ubyte ilRegisterMipNum(uint Num);
    ubyte ilRegisterNumFaces(uint Num);
    ubyte ilRegisterNumImages(uint Num);
    void ilRegisterOrigin(uint Origin);
    void ilRegisterPal(void *Pal, uint Size, uint Type);
    void ilRegisterType(uint Type);
    ubyte ilRemoveLoad(char * Ext);
    ubyte ilRemoveSave(char * Ext);
    void ilResetMemory(); // Deprecated
    void ilResetRead();
    void ilResetWrite();
    ubyte ilSave(uint Type, char * FileName);
    uint ilSaveF(uint Type, void * File);
    ubyte ilSaveImage(char * FileName);
    uint ilSaveL(uint Type, void *Lump, uint Size);
    ubyte ilSavePal(char * FileName);
    ubyte ilSetAlpha(double AlphaValue);
    ubyte ilSetData(void *Data);
    ubyte ilSetDuration(uint Duration);
    void ilSetInteger(uint Mode, int Param);
    void ilSetString(uint Mode, const char *String);
    void ilShutDown();
    ubyte ilSurfaceToDxtcData(uint Format);
    ubyte ilTexImage(uint Width, uint Height, uint Depth, ubyte NumChannels, uint Format, uint Type, void *Data);
    ubyte ilTexImageDxtc(int w, int h, int d, uint DxtFormat, const ubyte* data);
    uint ilTypeFromExt(char * FileName);
    ubyte ilTypeFunc(uint Mode);
    ubyte ilLoadData(char * FileName, uint Width, uint Height, uint Depth, ubyte Bpp);
    ubyte ilLoadDataF(void * File, uint Width, uint Height, uint Depth, ubyte Bpp);
    ubyte ilLoadDataL(void *Lump, uint Size, uint Width, uint Height, uint Depth, ubyte Bpp);
    ubyte ilSaveData(char * FileName);





    const ushort ILU_FILTER = 0x2600;
    const ushort ILU_NEAREST = 0x2601;
    const ushort ILU_LINEAR = 0x2602;
    const ushort ILU_BILINEAR = 0x2603;
    const ushort ILU_SCALE_BOX = 0x2604;
    const ushort ILU_SCALE_TRIANGLE = 0x2605;
    const ushort ILU_SCALE_BELL = 0x2606;
    const ushort ILU_SCALE_BSPLINE = 0x2607;
    const ushort ILU_SCALE_LANCZOS3 = 0x2608;
    const ushort ILU_SCALE_MITCHELL = 0x2609;


    // Error types
    const ushort ILU_INVALID_ENUM = 0x0501;
    const ushort ILU_OUT_OF_MEMORY = 0x0502;
    const ushort ILU_INTERNAL_ERROR = 0x0504;
    const ushort ILU_INVALID_VALUE = 0x0505;
    const ushort ILU_ILLEGAL_OPERATION = 0x0506;
    const ushort ILU_INVALID_PARAM = 0x0509;


    // Values
    const ushort ILU_PLACEMENT = 0x0700;
    const ushort ILU_LOWER_LEFT = 0x0701;
    const ushort ILU_LOWER_RIGHT = 0x0702;
    const ushort ILU_UPPER_LEFT = 0x0703;
    const ushort ILU_UPPER_RIGHT = 0x0704;
    const ushort ILU_CENTER = 0x0705;
    const ushort ILU_CONVOLUTION_MATRIX = 0x0710;


    // Languages
    const ushort ILU_ENGLISH = 0x0800;
    const ushort ILU_ARABIC = 0x0801;
    const ushort ILU_DUTCH = 0x0802;
    const ushort ILU_JAPANESE = 0x0803;
    const ushort ILU_SPANISH = 0x0804;
    const ushort ILU_GERMAN = 0x0805;
    const ushort ILU_FRENCH = 0x0806;

    bool iluAlienify();
    bool iluBlurAvg(uint Iter);
    bool iluBlurGaussian(uint Iter);
    bool iluBuildMipmaps();
    uint iluColoursUsed();
    bool iluCompareImage(uint Comp);
    bool iluContrast(float Contrast);
    bool iluCrop(uint XOff, uint YOff, uint ZOff, uint Width, uint Height, uint Depth);
    void iluDeleteImage(uint Id); // Deprecated
    bool iluEdgeDetectE();
    bool iluEdgeDetectP();
    bool iluEdgeDetectS();
    bool iluEmboss();
    bool iluEnlargeCanvas(uint Width, uint Height, uint Depth);
    bool iluEnlargeImage(float XDim, float YDim, float ZDim);
    bool iluEqualize();
    char * iluErrorString(int Error);
    bool iluConvolution(int *matrix, int scale, int bias);
    bool iluFlipImage();
    bool iluGammaCorrect(float Gamma);
    uint iluGenImage(); // Deprecated
    int iluGetInteger(int Mode);
    void iluGetIntegerv(int Mode, int *Param);
    char * iluGetString(int StringName);
    void iluImageParameter(int PName, int Param);
    void iluInit();
    bool iluInvertAlpha();
    uint iluLoadImage(char * FileName);
    bool iluMirror();
    bool iluNegative();
    bool iluNoisify(float Tolerance);
    bool iluPixelize(uint PixSize);
    bool iluReplaceColour(ubyte Red, ubyte Green, ubyte Blue, float Tolerance);
    bool iluRotate(float Angle);
    bool iluRotate3D(float x, float y, float z, float Angle);
    bool iluSaturate1f(float Saturation);
    bool iluSaturate4f(float r, float g, float b, float Saturation);
    bool iluScale(uint Width, uint Height, uint Depth);
    bool iluScaleAlpha(float scale);
    bool iluScaleColours(float r, float g, float b);
    bool iluSetLanguage(int Language);
    bool iluSharpen(float Factor, uint Iter);
    bool iluSwapColours();
    bool iluWave(float Angle);



    // Attribute Bits
    const uint ILUT_OPENGL_BIT = 0x00000001;
    const uint ILUT_D3D_BIT = 0x00000002;
    const uint ILUT_ALL_ATTRIB_BITS = 0x000FFFFF;


    // Error Types
    const ushort ILUT_INVALID_ENUM = 0x0501;
    const ushort ILUT_OUT_OF_MEMORY = 0x0502;
    const ushort ILUT_INVALID_VALUE = 0x0505;
    const ushort ILUT_ILLEGAL_OPERATION = 0x0506;
    const ushort ILUT_INVALID_PARAM = 0x0509;
    const ushort ILUT_COULD_NOT_OPEN_FILE = 0x050A;
    const ushort ILUT_STACK_OVERFLOW = 0x050E;
    const ushort ILUT_STACK_UNDERFLOW = 0x050F;
    const ushort ILUT_BAD_DIMENSIONS = 0x0511;
    const ushort ILUT_NOT_SUPPORTED = 0x0550;


    // State Definitions
    const ushort ILUT_PALETTE_MODE = 0x0600;
    const ushort ILUT_OPENGL_CONV = 0x0610;
    const ushort ILUT_D3D_MIPLEVELS = 0x0620;
    const ushort ILUT_MAXTEX_WIDTH = 0x0630;
    const ushort ILUT_MAXTEX_HEIGHT = 0x0631;
    const ushort ILUT_MAXTEX_DEPTH = 0x0632;
    const ushort ILUT_GL_USE_S3TC = 0x0634;
    const ushort ILUT_D3D_USE_DXTC = 0x0634;
    const ushort ILUT_GL_GEN_S3TC = 0x0635;
    const ushort ILUT_D3D_GEN_DXTC = 0x0635;
    const ushort ILUT_S3TC_FORMAT = 0x0705;
    const ushort ILUT_DXTC_FORMAT = 0x0705;
    const ushort ILUT_D3D_POOL = 0x0706;
    const ushort ILUT_D3D_ALPHA_KEY_COLOR = 0x0707;
    const ushort ILUT_D3D_ALPHA_KEY_COLOUR = 0x0707;
    const ushort ILUT_FORCE_INTEGER_FORMAT = 0x0636;

    //This new state does automatic texture target detection
    //if enabled. Currently, only cubemap detection is supported.
    //if the current image is no cubemap, the 2d texture is chosen.
    const ushort ILUT_GL_AUTODETECT_TEXTURE_TARGET = 0x0807;



    // The different rendering api's...more to be added later?
    const ubyte ILUT_OPENGL = 0;
    const ubyte ILUT_ALLEGRO = 1;
    const ubyte ILUT_WIN32 = 2;
    const ubyte ILUT_DIRECT3D8 = 3;
    const ubyte ILUT_DIRECT3D9 = 4;
    const ubyte ILUT_X11 = 5;
    const ubyte ILUT_DIRECT3D10 = 6;

    // ImageLib Utility Toolkit Functions
    bool ilutDisable(int Mode);
    bool ilutEnable(int Mode);
    bool ilutGetBoolean(int Mode);
    void ilutGetBooleanv(int Mode, bool *Param);
    int ilutGetInteger(int Mode);
    void ilutGetIntegerv(int Mode, int *Param);
    char * ilutGetString(int StringName);
    void ilutInit();
    bool ilutIsDisabled(int Mode);
    bool ilutIsEnabled(int Mode);
    void ilutPopAttrib();
    void ilutPushAttrib(uint Bits);
    void ilutSetInteger(int Mode, int Param);

    bool ilutRenderer(int Renderer);


    // ImageLib Utility Toolkit's OpenGL Functions
    uint ilutGLBindTexImage();
    uint ilutGLBindMipmaps();
    bool ilutGLBuildMipmaps();
    uint ilutGLLoadImage(char * FileName);
    bool ilutGLScreen();
    bool ilutGLScreenie();
    bool ilutGLSaveImage(char * FileName, uint TexID);
    bool ilutGLSubTex2D(uint TexID, uint XOff, uint YOff);
    bool ilutGLSubTex3D(uint TexID, uint XOff, uint YOff, uint ZOff);
    bool ilutGLSetTex2D(uint TexID);
    bool ilutGLSetTex3D(uint TexID);
    bool ilutGLTexImage(uint Level);
    bool ilutGLSubTex(uint TexID, uint XOff, uint YOff);

    bool ilutGLSetTex(uint TexID);  // Deprecated - use ilutGLSetTex2D.
    bool ilutGLSubTex(uint TexID, uint XOff, uint YOff);  // Use ilutGLSubTex2D.



}