extern (C) {

    /*
    * GLUT API macro definitions -- the special key codes:
    */
    const ushort GLUT_KEY_F1 = 0x0001;
    const ushort GLUT_KEY_F2 = 0x0002;
    const ushort GLUT_KEY_F3 = 0x0003;
    const ushort GLUT_KEY_F4 = 0x0004;
    const ushort GLUT_KEY_F5 = 0x0005;
    const ushort GLUT_KEY_F6 = 0x0006;
    const ushort GLUT_KEY_F7 = 0x0007;
    const ushort GLUT_KEY_F8 = 0x0008;
    const ushort GLUT_KEY_F9 = 0x0009;
    const ushort GLUT_KEY_F10 = 0x000A;
    const ushort GLUT_KEY_F11 = 0x000B;
    const ushort GLUT_KEY_F12 = 0x000C;
    const ushort GLUT_KEY_LEFT = 0x0064;
    const ushort GLUT_KEY_UP = 0x0065;
    const ushort GLUT_KEY_RIGHT = 0x0066;
    const ushort GLUT_KEY_DOWN = 0x0067;
    const ushort GLUT_KEY_PAGE_UP = 0x0068;
    const ushort GLUT_KEY_PAGE_DOWN = 0x0069;
    const ushort GLUT_KEY_HOME = 0x006A;
    const ushort GLUT_KEY_END = 0x006B;
    const ushort GLUT_KEY_INSERT = 0x006C;

    /*
    * GLUT API macro definitions -- mouse state definitions
    */
    const ushort GLUT_LEFT_BUTTON = 0x0000;
    const ushort GLUT_MIDDLE_BUTTON = 0x0001;
    const ushort GLUT_RIGHT_BUTTON = 0x0002;
    const ushort GLUT_DOWN = 0x0000;
    const ushort GLUT_UP = 0x0001;
    const ushort GLUT_LEFT = 0x0000;
    const ushort GLUT_ENTERED = 0x0001;

    /*
    * GLUT API macro definitions -- the display mode definitions
    */
    const ushort GLUT_RGB = 0x0000;
    const ushort GLUT_RGBA = 0x0000;
    const ushort GLUT_INDEX = 0x0001;
    const ushort GLUT_SINGLE = 0x0000;
    const ushort GLUT_DOUBLE = 0x0002;
    const ushort GLUT_ACCUM = 0x0004;
    const ushort GLUT_ALPHA = 0x0008;
    const ushort GLUT_DEPTH = 0x0010;
    const ushort GLUT_STENCIL = 0x0020;
    const ushort GLUT_MULTISAMPLE = 0x0080;
    const ushort GLUT_STEREO = 0x0100;
    const ushort GLUT_LUMINANCE = 0x0200;

    /*
    * GLUT API macro definitions -- windows and menu related definitions
    */
    const ushort GLUT_MENU_NOT_IN_USE = 0x0000;
    const ushort GLUT_MENU_IN_USE = 0x0001;
    const ushort GLUT_NOT_VISIBLE = 0x0000;
    const ushort GLUT_VISIBLE = 0x0001;
    const ushort GLUT_HIDDEN = 0x0000;
    const ushort GLUT_FULLY_RETAINED = 0x0001;
    const ushort GLUT_PARTIALLY_RETAINED = 0x0002;
    const ushort GLUT_FULLY_COVERED = 0x0003;

    /*
    * GLUT API macro definitions -- the glutGet parameters
    */
    const ushort GLUT_WINDOW_X = 0x0064;
    const ushort GLUT_WINDOW_Y = 0x0065;
    const ushort GLUT_WINDOW_WIDTH = 0x0066;
    const ushort GLUT_WINDOW_HEIGHT = 0x0067;
    const ushort GLUT_WINDOW_BUFFER_SIZE = 0x0068;
    const ushort GLUT_WINDOW_STENCIL_SIZE = 0x0069;
    const ushort GLUT_WINDOW_DEPTH_SIZE = 0x006A;
    const ushort GLUT_WINDOW_RED_SIZE = 0x006B;
    const ushort GLUT_WINDOW_GREEN_SIZE = 0x006C;
    const ushort GLUT_WINDOW_BLUE_SIZE = 0x006D;
    const ushort GLUT_WINDOW_ALPHA_SIZE = 0x006E;
    const ushort GLUT_WINDOW_ACCUM_RED_SIZE = 0x006F;
    const ushort GLUT_WINDOW_ACCUM_GREEN_SIZE = 0x0070;
    const ushort GLUT_WINDOW_ACCUM_BLUE_SIZE = 0x0071;
    const ushort GLUT_WINDOW_ACCUM_ALPHA_SIZE = 0x0072;
    const ushort GLUT_WINDOW_DOUBLEBUFFER = 0x0073;
    const ushort GLUT_WINDOW_RGBA = 0x0074;
    const ushort GLUT_WINDOW_PARENT = 0x0075;
    const ushort GLUT_WINDOW_NUM_CHILDREN = 0x0076;
    const ushort GLUT_WINDOW_COLORMAP_SIZE = 0x0077;
    const ushort GLUT_WINDOW_NUM_SAMPLES = 0x0078;
    const ushort GLUT_WINDOW_STEREO = 0x0079;
    const ushort GLUT_WINDOW_CURSOR = 0x007A;

    const ushort GLUT_SCREEN_WIDTH = 0x00C8;
    const ushort GLUT_SCREEN_HEIGHT = 0x00C9;
    const ushort GLUT_SCREEN_WIDTH_MM = 0x00CA;
    const ushort GLUT_SCREEN_HEIGHT_MM = 0x00CB;
    const ushort GLUT_MENU_NUM_ITEMS = 0x012C;
    const ushort GLUT_DISPLAY_MODE_POSSIBLE = 0x0190;
    const ushort GLUT_INIT_WINDOW_X = 0x01F4;
    const ushort GLUT_INIT_WINDOW_Y = 0x01F5;
    const ushort GLUT_INIT_WINDOW_WIDTH = 0x01F6;
    const ushort GLUT_INIT_WINDOW_HEIGHT = 0x01F7;
    const ushort GLUT_INIT_DISPLAY_MODE = 0x01F8;
    const ushort GLUT_ELAPSED_TIME = 0x02BC;
    const ushort GLUT_WINDOW_FORMAT_ID = 0x007B;

    /*
    * GLUT API macro definitions -- the glutDeviceGet parameters
    */
    const ushort GLUT_HAS_KEYBOARD = 0x0258;
    const ushort GLUT_HAS_MOUSE = 0x0259;
    const ushort GLUT_HAS_SPACEBALL = 0x025A;
    const ushort GLUT_HAS_DIAL_AND_BUTTON_BOX = 0x025B;
    const ushort GLUT_HAS_TABLET = 0x025C;
    const ushort GLUT_NUM_MOUSE_BUTTONS = 0x025D;
    const ushort GLUT_NUM_SPACEBALL_BUTTONS = 0x025E;
    const ushort GLUT_NUM_BUTTON_BOX_BUTTONS = 0x025F;
    const ushort GLUT_NUM_DIALS = 0x0260;
    const ushort GLUT_NUM_TABLET_BUTTONS = 0x0261;
    const ushort GLUT_DEVICE_IGNORE_KEY_REPEAT = 0x0262;
    const ushort GLUT_DEVICE_KEY_REPEAT = 0x0263;
    const ushort GLUT_HAS_JOYSTICK = 0x0264;
    const ushort GLUT_OWNS_JOYSTICK = 0x0265;
    const ushort GLUT_JOYSTICK_BUTTONS = 0x0266;
    const ushort GLUT_JOYSTICK_AXES = 0x0267;
    const ushort GLUT_JOYSTICK_POLL_RATE = 0x0268;

    /*
    * GLUT API macro definitions -- the glutLayerGet parameters
    */
    const ushort GLUT_OVERLAY_POSSIBLE = 0x0320;
    const ushort GLUT_LAYER_IN_USE = 0x0321;
    const ushort GLUT_HAS_OVERLAY = 0x0322;
    const ushort GLUT_TRANSPARENT_INDEX = 0x0323;
    const ushort GLUT_NORMAL_DAMAGED = 0x0324;
    const ushort GLUT_OVERLAY_DAMAGED = 0x0325;

    /*
    * GLUT API macro definitions -- the glutVideoResizeGet parameters
    */
    const ushort GLUT_VIDEO_RESIZE_POSSIBLE = 0x0384;
    const ushort GLUT_VIDEO_RESIZE_IN_USE = 0x0385;
    const ushort GLUT_VIDEO_RESIZE_X_DELTA = 0x0386;
    const ushort GLUT_VIDEO_RESIZE_Y_DELTA = 0x0387;
    const ushort GLUT_VIDEO_RESIZE_WIDTH_DELTA = 0x0388;
    const ushort GLUT_VIDEO_RESIZE_HEIGHT_DELTA = 0x0389;
    const ushort GLUT_VIDEO_RESIZE_X = 0x038A;
    const ushort GLUT_VIDEO_RESIZE_Y = 0x038B;
    const ushort GLUT_VIDEO_RESIZE_WIDTH = 0x038C;
    const ushort GLUT_VIDEO_RESIZE_HEIGHT = 0x038D;

    /*
    * GLUT API macro definitions -- the glutUseLayer parameters
    */
    const ushort GLUT_NORMAL = 0x0000;
    const ushort GLUT_OVERLAY = 0x0001;

    /*
    * GLUT API macro definitions -- the glutGetModifiers parameters
    */
    const ushort GLUT_ACTIVE_SHIFT = 0x0001;
    const ushort GLUT_ACTIVE_CTRL = 0x0002;
    const ushort GLUT_ACTIVE_ALT = 0x0004;

    /*
    * GLUT API macro definitions -- the glutSetCursor parameters
    */
    const ushort GLUT_CURSOR_RIGHT_ARROW = 0x0000;
    const ushort GLUT_CURSOR_LEFT_ARROW = 0x0001;
    const ushort GLUT_CURSOR_INFO = 0x0002;
    const ushort GLUT_CURSOR_DESTROY = 0x0003;
    const ushort GLUT_CURSOR_HELP = 0x0004;
    const ushort GLUT_CURSOR_CYCLE = 0x0005;
    const ushort GLUT_CURSOR_SPRAY = 0x0006;
    const ushort GLUT_CURSOR_WAIT = 0x0007;
    const ushort GLUT_CURSOR_TEXT = 0x0008;
    const ushort GLUT_CURSOR_CROSSHAIR = 0x0009;
    const ushort GLUT_CURSOR_UP_DOWN = 0x000A;
    const ushort GLUT_CURSOR_LEFT_RIGHT = 0x000B;
    const ushort GLUT_CURSOR_TOP_SIDE = 0x000C;
    const ushort GLUT_CURSOR_BOTTOM_SIDE = 0x000D;
    const ushort GLUT_CURSOR_LEFT_SIDE = 0x000E;
    const ushort GLUT_CURSOR_RIGHT_SIDE = 0x000F;
    const ushort GLUT_CURSOR_TOP_LEFT_CORNER = 0x0010;
    const ushort GLUT_CURSOR_TOP_RIGHT_CORNER = 0x0011;
    const ushort GLUT_CURSOR_BOTTOM_RIGHT_CORNER = 0x0012;
    const ushort GLUT_CURSOR_BOTTOM_LEFT_CORNER = 0x0013;
    const ushort GLUT_CURSOR_INHERIT = 0x0064;
    const ushort GLUT_CURSOR_NONE = 0x0065;
    const ushort GLUT_CURSOR_FULL_CROSSHAIR = 0x0066;

    /*
    * GLUT API macro definitions -- RGB color component specification definitions
    */
    const ushort GLUT_RED = 0x0000;
    const ushort GLUT_GREEN = 0x0001;
    const ushort GLUT_BLUE = 0x0002;

    /*
    * GLUT API macro definitions -- additional keyboard and joystick definitions
    */
    const ushort GLUT_KEY_REPEAT_OFF = 0x0000;
    const ushort GLUT_KEY_REPEAT_ON = 0x0001;
    const ushort GLUT_KEY_REPEAT_DEFAULT = 0x0002;

    const ushort GLUT_JOYSTICK_BUTTON_A = 0x0001;
    const ushort GLUT_JOYSTICK_BUTTON_B = 0x0002;
    const ushort GLUT_JOYSTICK_BUTTON_C = 0x0004;
    const ushort GLUT_JOYSTICK_BUTTON_D = 0x0008;

    /*
    * GLUT API macro definitions -- game mode definitions
    */
    const ushort GLUT_GAME_MODE_ACTIVE = 0x0000;
    const ushort GLUT_GAME_MODE_POSSIBLE = 0x0001;
    const ushort GLUT_GAME_MODE_WIDTH = 0x0002;
    const ushort GLUT_GAME_MODE_HEIGHT = 0x0003;
    const ushort GLUT_GAME_MODE_PIXEL_DEPTH = 0x0004;
    const ushort GLUT_GAME_MODE_REFRESH_RATE = 0x0005;
    const ushort GLUT_GAME_MODE_DISPLAY_CHANGED = 0x0006;

    /*
    * Initialization functions, see fglut_init.c
    */
    void  glutInit( int* pargc, char** argv );
    void  glutInitWindowPosition( int x, int y );
    void  glutInitWindowSize( int width, int height );
    void  glutInitDisplayMode( uint displayMode );
    void  glutInitDisplayString( const char* displayMode );

    /*
    * Process loop function, see freeglut_main.c
    */
    void  glutMainLoop( );

    /*
    * Window management functions, see freeglut_window.c
    */
    int   glutCreateWindow( const char* title );
    int   glutCreateSubWindow( int window, int x, int y, int width, int height );
    void  glutDestroyWindow( int window );
    void  glutSetWindow( int window );
    int   glutGetWindow( );
    void  glutSetWindowTitle( const char* title );
    void  glutSetIconTitle( const char* title );
    void  glutReshapeWindow( int width, int height );
    void  glutPositionWindow( int x, int y );
    void  glutShowWindow( );
    void  glutHideWindow( );
    void  glutIconifyWindow( );
    void  glutPushWindow( );
    void  glutPopWindow( );
    void  glutFullScreen( );

    /*
    * Display-connected functions, see freeglut_display.c
    */
    void  glutPostWindowRedisplay( int window );
    void  glutPostRedisplay( );
    void  glutSwapBuffers( );

    /*
    * Mouse cursor functions, see freeglut_cursor.c
    */
    void  glutWarpPointer( int x, int y );
    void  glutSetCursor( int cursor );

    /*
    * Overlay stuff, see freeglut_overlay.c
    */
    void  glutEstablishOverlay( );
    void  glutRemoveOverlay( );
    void  glutUseLayer( int layer );
    void  glutPostOverlayRedisplay( );
    void  glutPostWindowOverlayRedisplay( int window );
    void  glutShowOverlay( );
    void  glutHideOverlay( );

    /*
    * Menu stuff, see freeglut_menu.c
    */
    int   glutCreateMenu( void function( int menu ) );
    void  glutDestroyMenu( int menu );
    int   glutGetMenu( );
    void  glutSetMenu( int menu );
    void  glutAddMenuEntry( const char* label, int value );
    void  glutAddSubMenu( const char* label, int subMenu );
    void  glutChangeToMenuEntry( int item, const char* label, int value );
    void  glutChangeToSubMenu( int item, const char* label, int value );
    void  glutRemoveMenuItem( int item );
    void  glutAttachMenu( int button );
    void  glutDetachMenu( int button );

    /*
    * Global callback functions, see freeglut_callbacks.c
    */
    void  glutTimerFunc( uint time, void function( int ), int value );
    void  glutIdleFunc( void function( ) );

    /*
    * Window-specific callback functions, see freeglut_callbacks.c
    */
    void  glutKeyboardFunc( void function( ubyte, int, int ) );
    void  glutSpecialFunc( void function( int, int, int ) );
    void  glutReshapeFunc( void function( int, int ) );
    void  glutVisibilityFunc( void function( int ) );
    void  glutDisplayFunc( void function( ) );
    void  glutMouseFunc( void function( int, int, int, int ) );
    void  glutMotionFunc( void function( int, int ) );
    void  glutPassiveMotionFunc( void function( int, int ) );
    void  glutEntryFunc( void function( int ) );

    void  glutKeyboardUpFunc( void function( ubyte, int, int ) );
    void  glutSpecialUpFunc( void function( int, int, int ) );
    void  glutJoystickFunc( void function( uint, int, int, int ), int pollInterval );
    void  glutMenuStateFunc( void function( int ) );
    void  glutMenuStatusFunc( void function( int, int, int ) );
    void  glutOverlayDisplayFunc( void function( ) );
    void  glutWindowStatusFunc( void function( int ) );

    void  glutSpaceballMotionFunc( void function( int, int, int ) );
    void  glutSpaceballRotateFunc( void function( int, int, int ) );
    void  glutSpaceballButtonFunc( void function( int, int ) );
    void  glutButtonBoxFunc( void function( int, int ) );
    void  glutDialsFunc( void function( int, int ) );
    void  glutTabletMotionFunc( void function( int, int ) );
    void  glutTabletButtonFunc( void function( int, int, int, int ) );

    /*
    * State setting and retrieval functions, see freeglut_state.c
    */
    int   glutGet( int query );
    int   glutDeviceGet( int query );
    int   glutGetModifiers( );
    int   glutLayerGet( int query );

    /*
    * Font stuff, see freeglut_font.c
    */
    void  glutBitmapCharacter( void* font, int character );
    int   glutBitmapWidth( void* font, int character );
    void  glutStrokeCharacter( void* font, int character );
    int   glutStrokeWidth( void* font, int character );
    int   glutBitmapLength( void* font, const ubyte* string );
    int   glutStrokeLength( void* font, const ubyte* string );

    /*
    * Geometry functions, see freeglut_geometry.c
    */
    void  glutWireCube( double size );
    void  glutSolidCube( double size );
    void  glutWireSphere( double radius, int slices, int stacks );
    void  glutSolidSphere( double radius, int slices, int stacks );
    void  glutWireCone( double base, double height, int slices, int stacks );
    void  glutSolidCone( double base, double height, int slices, int stacks );

    void  glutWireTorus( double innerRadius, double outerRadius, int sides, int rings );
    void  glutSolidTorus( double innerRadius, double outerRadius, int sides, int rings );
    void  glutWireDodecahedron( );
    void  glutSolidDodecahedron( );
    void  glutWireOctahedron( );
    void  glutSolidOctahedron( );
    void  glutWireTetrahedron( );
    void  glutSolidTetrahedron( );
    void  glutWireIcosahedron( );
    void  glutSolidIcosahedron( );

    /*
    * Teapot rendering functions, found in freeglut_teapot.c
    */
    void  glutWireTeapot( double size );
    void  glutSolidTeapot( double size );

    /*
    * Game mode functions, see freeglut_gamemode.c
    */
    void  glutGameModeString( const char* string );
    int   glutEnterGameMode( );
    void  glutLeaveGameMode( );
    int   glutGameModeGet( int query );

    /*
    * Video resize functions, see freeglut_videoresize.c
    */
    int   glutVideoResizeGet( int query );
    void  glutSetupVideoResizing( );
    void  glutStopVideoResizing( );
    void  glutVideoResize( int x, int y, int width, int height );
    void  glutVideoPan( int x, int y, int width, int height );

    /*
    * Colormap functions, see freeglut_misc.c
    */
    void  glutSetColor( int color, float red, float green, float blue );
    float glutGetColor( int color, int component );
    void  glutCopyColormap( int window );

    /*
    * Misc keyboard and joystick functions, see freeglut_misc.c
    */
    void  glutIgnoreKeyRepeat( int ignore );
    void  glutSetKeyRepeat( int repeatMode );
    void  glutForceJoystickFunc( );

    /*
    * Misc functions, see freeglut_misc.c
    */
    int   glutExtensionSupported( const char* extension );
    void  glutReportErrors( );

}