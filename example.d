import wrapper.gl;
import wrapper.glut;
import wrapper.il;


int window_w = 640;
int window_h = 480;

extern (C) void displayCallback() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(1.0, 1.0, 1.0, 0.0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable (GL_TEXTURE_2D);
    glEnable(GL_BLEND);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0 , window_w, window_h, 0, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glutSwapBuffers();
    glFlush();
}

/**
 * Some basic callbacks, too lazy to edit
 */
extern (C) void idleCallback() {
    glutPostRedisplay();
}
extern (C) void reshapeCallback(int w, int h) {
    window_w = w;
    window_h = h;
    glViewport(0, 0, w, h);
}
extern (C) void mouseStateCallback(int button, int state, int x, int y) {
}

extern (C) void specialKeyCallback(int key, int x, int y) {
}

extern (C) void keyboardUpCallback(ubyte key, int x, int y) {
}

extern (C) void specialKeyUpCallback(int key, int x, int y) {
}

extern (C) void keyboardCallback(ubyte key, int x, int y) {
}


/**
 * Will create a white empty window
 */
int main() {
    /**
     * GLUT initialization
     */
    int argc = 0; // FIXME... reason to do that is quite obvious...
    glutInit(&argc, null);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(window_w, window_h);
    glutCreateWindow("Hello World!\0");

    /* Setting the callbacks */
    glutDisplayFunc(&displayCallback);
    glutIdleFunc(&idleCallback);
    glutReshapeFunc(&reshapeCallback);
    glutMouseFunc(&mouseStateCallback);
    glutSpecialFunc(&specialKeyCallback);
    glutSpecialUpFunc(&specialKeyUpCallback);
    glutKeyboardUpFunc(&keyboardUpCallback);
    glutKeyboardFunc(&keyboardCallback);

    /**
     * DevIL initialization
     */
    ilInit();
    ilutRenderer(ILUT_OPENGL);

    glutMainLoop();
    return 0;
}